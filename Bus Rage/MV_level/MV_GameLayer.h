//
//  MV_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "MV_HousesLayer.h"
#import "MV_FenceLayer.h"
#import "MV_RoadLayer.h"
#import "MV_TreesLayer.h"
#import "MV_HillsLayer.h"
#import "MV_Horizon1Layer.h"
#import "MV_BaseLayer.h"

@interface MV_GameLayer : CCLayer {
    
    MV_HousesLayer *mv_houseLayer;
    MV_FenceLayer *mv_fenceLayer;
    MV_RoadLayer *mv_roadLayer;
    MV_TreesLayer *mv_treeLayer;
    MV_HillsLayer *mv_hillLayer;
    MV_Horizon1Layer *mv_horizon1Layer;
    MV_BaseLayer *mv_baseLayer;
    CCSprite *cloud_dustSprite;
    int m_timecount;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
