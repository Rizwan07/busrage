//
//  MV_HousesLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import "MV_HousesLayer.h"
#import "MainLayer.h"
#define SHOW_LIMIT 300

@implementation MV_HousesLayer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        m_Objects = [NSArray arrayWithObjects://MV:0,1,5,6,10,11,13,14,15,16,17
                     
                     @"14, 25",//tree1
                     @"0, 40",//house1                    
                     @"10, 70",//ditch1
                     @"15, 120",//tree2
                     @"16, 150",//rock1                     
                     @"13, 190",//gas
                     @"1, 220",//house2  
                     @"17, 260",//rock2
                     @"11, 270",//ditch2                     
                     
                     @"5, 340",//billboard1                     
                     @"0, 375",//house1
                     @"10, 430",//ditch1
                     @"14, 475",//tree1
                     @"16, 490",//rock1
                     @"1, 520",//house2 
                     @"15, 560",//tree2
                     @"11, 570",//ditch2
                     
                     //@"5, 640",//billboard1 
                     @"17, 680",//rock2
                     //@"6, 720",//billboard2                     
                     @"14, 750",//tree1
                     @"13, 800",//gas
                     @"16, 845",//rock1
                     @"15, 860",//tree2
                     @"10, 890",//ditch1 
                     //@"5, 940",//billboard1 
                     @"11, 960",//ditch2 
                     @"17, 990",//rock2
                     @"0, 1030",//house1
                     //@"6, 1060",//billboard2
                     @"1, 1100",//house2
                     nil];
        
        int house1_index = 0;
        int house2_index = 0;
        int gasStation_index = 0;
        int billboard1_index = 0;
        int billboard2_index = 0;
        int ditch1_index = 0;
        int ditch2_index = 0;
        int tree1_index = 0;
        int tree2_index = 0;
        int rock1_index = 0;
        int rock2_index = 0;
        for (int i = 0; i < [m_Objects count]; i++ )
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case H_OINFO_HOUSE1:                
                    m_House1[house1_index] = [[params objectAtIndex:1] intValue];
                    house1_index++;             
                    break;	
                case H_OINFO_HOUSE2:                
                    m_House2[house2_index] = [[params objectAtIndex:1] intValue];
                    house2_index++;                
                    break;
                case H_OINFO_GASSTATION:
                    m_GasStation[gasStation_index] = [[params objectAtIndex:1] intValue];
                    gasStation_index++;
                    break;
                case H_OINFO_BILLBOARD1:                
                    m_BillBoard1[billboard1_index] = [[params objectAtIndex:1] intValue];
                    billboard1_index++;                
                    break;	
                case H_OINFO_BILLBOARD2:                
                    m_BillBoard2[billboard2_index] = [[params objectAtIndex:1] intValue];
                    billboard2_index++;                
                    break;
                case H_OINFO_DITCH1:                
                    m_Ditch1[ditch1_index] = [[params objectAtIndex:1] intValue];
                    ditch1_index++;                
                    break;
                case H_OINFO_DITCH2:                
                    m_Ditch2[ditch2_index] = [[params objectAtIndex:1] intValue];
                    ditch2_index++;                
                    break;
                case H_OINFO_TREE1:                
                    m_Tree1[tree1_index] = [[params objectAtIndex:1] intValue];
                    tree1_index++;                
                    break;
                case H_OINFO_TREE2:                
                    m_Tree2[tree2_index] = [[params objectAtIndex:1] intValue];
                    tree2_index++; 
                    break;
                case H_OINFO_ROCK1:                
                    m_Rock1[rock1_index] = [[params objectAtIndex:1] intValue];
                    rock1_index++;                
                    break;
                case H_OINFO_ROCK2:                
                    m_Rock2[rock2_index] = [[params objectAtIndex:1] intValue];
                    rock2_index++;  
                    break;
            }
        }
        
        houseSprite1 = [CCSprite spriteWithFile:@"house_1_MV.png"];
        [self addChild:houseSprite1 z:2];
        houseSprite1.visible = NO;  
        
        houseSprite2 = [CCSprite spriteWithFile:@"house_2_MV.png"];
        [self addChild:houseSprite2 z:1];
        houseSprite2.visible = NO;
        
        gasStationSprite = [CCSprite spriteWithFile:@"gas station_MV.png"];
        [self addChild:gasStationSprite z:3];
        gasStationSprite.visible = NO;
                
        billBoard1Sprite = [CCSprite spriteWithFile:@"Boarding_MV.png"];
        [self addChild:billBoard1Sprite z:2];
        billBoard1Sprite.visible = NO;
        billBoard2Sprite = [CCSprite spriteWithFile:@"Boarding_MV.png"];
        [self addChild:billBoard2Sprite z:1];
        billBoard2Sprite.visible = NO;
        
        ditch1Sprite = [CCSprite spriteWithFile:@"Pond_MV.png"];
        [self addChild:ditch1Sprite z:0];
        ditch1Sprite.visible = NO;
        
        ditch2Sprite = [CCSprite spriteWithFile:@"Pond_MV.png"];
        [self addChild:ditch2Sprite z:0];
        ditch2Sprite.visible = NO;
        
        tree1Sprite = [CCSprite spriteWithFile:@"tree1_MV.png"];
        [self addChild:tree1Sprite z:3];
        tree1Sprite.visible = NO;
        
        tree2Sprite = [CCSprite spriteWithFile:@"tree2_MV.png"];
        [self addChild:tree2Sprite z:3];
        tree2Sprite.visible = NO;
        
        rock1Sprite = [CCSprite spriteWithFile:@"Rock_1_MV.png"];
        [self addChild:rock1Sprite z:0];
        rock1Sprite.visible = NO;
        
        rock2Sprite = [CCSprite spriteWithFile:@"Rock_2_MV.png"];
        [self addChild:rock2Sprite z:0];
        rock2Sprite.visible = NO;
        
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    [self showHouse1:runDistance];
    [self showHouse2:runDistance];
    [self showGasStation:runDistance];
    [self showBillBoard1:runDistance];
    [self showBillBoard2:runDistance];
    [self showDitch1:runDistance];
    [self showDitch2:runDistance];
    [self showTree1:runDistance];
    [self showTree2:runDistance];
    [self showRock1:runDistance];
    [self showRock2:runDistance];
}
- (void) showHouse1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_House1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    houseSprite1.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        houseSprite1.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        houseSprite1.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        houseSprite1.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showHouse2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_House2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    houseSprite2.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        houseSprite2.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        houseSprite2.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        houseSprite2.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showGasStation:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_GasStation[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    gasStationSprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        gasStationSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        gasStationSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        gasStationSprite.scale = 1.5 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showBillBoard1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_BillBoard1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    billBoard1Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        billBoard1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        billBoard1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        billBoard1Sprite.scale = 1.5 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showBillBoard2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_BillBoard2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    billBoard2Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        billBoard2Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        billBoard2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        billBoard2Sprite.scale = 1.5 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showDitch1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Ditch1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    ditch1Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        ditch1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        ditch1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        ditch1Sprite.scale = 1.0 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }

}
- (void) showDitch2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Ditch2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    ditch2Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        ditch2Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        ditch2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        ditch2Sprite.scale = 1.0 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showTree1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Tree1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    tree1Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        tree1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        tree1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        tree1Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showTree2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Tree2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    tree2Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        tree2Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        tree2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        tree2Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showRock1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Rock1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    rock1Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        rock1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        rock1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        rock1Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showRock2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Rock2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    rock2Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        rock2Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        rock2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        rock2Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
@end
