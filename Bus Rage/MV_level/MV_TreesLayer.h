//
//  MV_TreesLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface MV_TreesLayer : CCLayer {
    CCSprite *tree1Sprite;
    CCSprite *tree2Sprite;
    CCSprite *bush1Sprite;
    CCSprite *bush2Sprite;
    CCSprite *rock1Sprite;
    CCSprite *rock2Sprite;
    int m_Tree1[3];
    int m_Tree2[3];
    int m_Bush1[3];
    int m_Bush2[3];
    int m_Rock1[3];
    int m_Rock2[3];
    
    NSArray*    m_Objects; 
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) showTree1:(int)runDistance;
- (void) showTree2:(int)runDistance;
- (void) showBush1:(int)runDistance;
- (void) showBush2:(int)runDistance;
- (void) showRock1:(int)runDistance;
- (void) showRock2:(int)runDistance;
@end
