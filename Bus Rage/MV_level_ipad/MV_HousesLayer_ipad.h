//
//  CS_HousesLayer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

@interface MV_HousesLayer_ipad : CCLayer {
    
    CCSprite *houseSprite1;
    CCSprite *houseSprite2;
    CCSprite *gasStationSprite;
    CCSprite *billBoard1Sprite, *billBoard2Sprite; 
    CCSprite *ditch1Sprite, *ditch2Sprite;
    CCSprite *tree1Sprite, *tree2Sprite;
    CCSprite *rock1Sprite, *rock2Sprite;
    NSArray *m_Objects;
    int m_House1[3];
    int m_House2[3];
    int m_GasStation[3];
    int m_BillBoard1[3], m_BillBoard2[3];
    int m_Ditch1[3];
    int m_Ditch2[3];
    int m_Tree1[3];
    int m_Tree2[3];
    int m_Rock1[3];
    int m_Rock2[3];
}

- (void) runMove:(int)runDistance speed:(int)speed;
- (void) showHouse1:(int)runDistance;
- (void) showHouse2:(int)runDistance;
- (void) showGasStation:(int)runDistance;
- (void) showBillBoard1:(int)runDistance;
- (void) showBillBoard2:(int)runDistance;
- (void) showDitch1:(int)runDistance;
- (void) showDitch2:(int)runDistance;
- (void) showTree1:(int)runDistance;
- (void) showTree2:(int)runDistance;
- (void) showRock1:(int)runDistance;
- (void) showRock2:(int)runDistance;
@end
