//
//  CS_HillsLayer_ipad.m
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "MV_HillsLayer_ipad.h"

@implementation MV_HillsLayer_ipad
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        hill1Sprite = [CCSprite spriteWithFile:@"Mountain_1_MV_ipad.png"];
        [self addChild:hill1Sprite z:2];        
        hill1Sprite.visible = YES;
        hill1Sprite.position = ccp(250*2.1333333, 144*2.4);
        hill1Sprite.scale = 0.4;
        
        hill2Sprite = [CCSprite spriteWithFile:@"Mountain_2_MV_ipad.png"];
        [self addChild:hill2Sprite z:1];        
        hill2Sprite.visible = YES;
        hill2Sprite.position = ccp(100*2.1333333, 140*2.4);
        hill2Sprite.scale = 1.2;
        
        hill3Sprite = [CCSprite spriteWithFile:@"Mountain_3_MV_ipad.png"];
        [self addChild:hill3Sprite z:0];        
        hill3Sprite.visible = YES;
        hill3Sprite.position = ccp(200*2.1333333, 148*2.4);
        hill3Sprite.scale = 0.8;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    if (runDistance < 10) {
        hill1Sprite.position = ccp(250*2.1333333, 144*2.4);
        hill2Sprite.position = ccp(100*2.1333333, 140*2.4);
        hill3Sprite.position = ccp(200*2.1333333, 148*2.4);
    }
    if (speed > 0) {
        CGPoint pos;
        pos = hill1Sprite.position;
        pos.x -= 0.4 * speed/2;
        hill1Sprite.position = pos; 
        
        pos = hill2Sprite.position;
        pos.x -= 0.4 * speed/2;
        hill2Sprite.position = pos; 
        
        pos = hill3Sprite.position;
        pos.x -= 0.4 * speed/2;
        hill3Sprite.position = pos; 
    }
    
}

@end
