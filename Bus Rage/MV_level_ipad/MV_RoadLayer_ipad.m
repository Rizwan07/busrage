//
//  CS_RoadLayer_ipad.m
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "MV_RoadLayer_ipad.h"

@implementation MV_RoadLayer_ipad
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {        
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        road1Sprite = [CCSprite spriteWithFile:@"road_MV_01_ipad.png"];
        [self addChild:road1Sprite z:1];   
        road2Sprite = [CCSprite spriteWithFile:@"road_MV_02_ipad.png"];
        [self addChild:road2Sprite z:1]; 
        road3Sprite = [CCSprite spriteWithFile:@"road_MV_03_ipad.png"];
        [self addChild:road3Sprite z:1]; 
        road4Sprite = [CCSprite spriteWithFile:@"road_MV_04_ipad.png"];
        [self addChild:road4Sprite z:1]; 
        road1Sprite.position = road2Sprite.position = road3Sprite.position = road4Sprite.position = ccp(size.width/2, (160*2.4)+4);
        road1Sprite.visible = road2Sprite.visible = road3Sprite.visible = road4Sprite.visible = NO;
        road1Sprite.scale = road2Sprite.scale = road3Sprite.scale = road4Sprite.scale = 2.05;
        road1Sprite.visible = YES;
        
        current_road = 1;  
        runCount = 0;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}

- (void) changeRoadSprite
{
    current_road++;
    if (current_road == 5)
        current_road = 1;
    road1Sprite.visible = road2Sprite.visible = road3Sprite.visible = road4Sprite.visible = NO;
    switch (current_road) {
        case 1:
            road1Sprite.visible = YES;
            break;
        case 2:
            road2Sprite.visible = YES;
            break;
        case 3:
            road3Sprite.visible = YES;
            break;
        case 4:
            road4Sprite.visible = YES;
            break;
        default:
            break;
    }
}

- (void) runMove:(int)speed
{
    runCount++;
    if (runCount > 40000)
        runCount = 0;
    
    if (speed == 0)
        return;
    
    if (speed == 10) {
        [self changeRoadSprite];
    }
    if (speed == 8) {
        if (runCount % 2 == 0) {
            [self changeRoadSprite];
        }
    }
    if (speed == 6) {
        if (runCount % 2 == 0) {
            [self changeRoadSprite];
        }
    }
    if (speed == 4) {
        if (runCount % 2 == 0) {
            [self changeRoadSprite];
        }
    }
    if (speed == 2) {
        if (runCount % 4 == 0) {
            [self changeRoadSprite];
        }
    }    
    
//    if (speed != 0) {
//        
//        if (current_ground == 0) {
//            current_ground = 1;
//            ground1Sprite.visible = NO; 
//            ground2Sprite.visible = YES;
//        } else {
//            current_ground = 0;
//            ground1Sprite.visible = YES; 
//            ground2Sprite.visible = NO;
//        }
//    }
    
}

@end
