//
//  HowToViewController.h
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleAudioEngine.h"

@interface HowToViewController : UIViewController
- (IBAction)backTapped:(id)sender;

@end
