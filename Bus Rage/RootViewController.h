//
//  RootViewController.h
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright k 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CutscenePlayer.h"
//#import "FBConnect.h"
#import "FBConnectSingleton.h"

@interface RootViewController : UIViewController <ScenePlayerDelegate, FBConnectDelegate, UIAlertViewDelegate>
{
    IBOutlet UIButton *homeButton;
    BOOL m_bAccelerator, m_bBrake, m_bSteeringLeft, m_bSteeringRight;
    BOOL m_bBusTargetHit;
    BOOL m_bMessageShow;
    BOOL m_bStop;
    NSArray *m_imageObjects;
    NSTimer *checkXTimer;
    //Facebook* faceBookR;
    FBConnectSingleton *fbConnect;
    bool m_bGameEnd;
    BOOL isFirstCall;
    BOOL isCutSceen;

    IBOutlet UIImageView *driverImage;
    IBOutlet UIImageView *target1Image;
    IBOutlet UIImageView *target2Image;
    IBOutlet UIImageView *target3Image;
    IBOutlet UIImageView *target4Image;
    IBOutlet UIImageView *target5Image;
    IBOutlet UIImageView *target6Image;
    IBOutlet UIImageView *target7Image;
    
    IBOutlet UIView *viewForBillBoard;
}
//@property (retain, nonatomic) IBOutlet UIButton *homeButton;
@property (assign) BOOL m_bAccelerator;
@property (assign) BOOL m_bBrake;
@property (assign) BOOL m_bSteeringLeft;
@property (assign) BOOL m_bSteeringRight;
@property (assign) BOOL m_bBusTargetHit;
@property (assign) BOOL m_bMessageShow;
@property (assign) BOOL m_bStop;

@property (nonatomic, retain) IBOutlet UIImageView *target1ImageX;
@property (nonatomic, retain) IBOutlet UIImageView *target2ImageX;
@property (nonatomic, retain) IBOutlet UIImageView *target3ImageX;
@property (nonatomic, retain) IBOutlet UIImageView *target4ImageX;
@property (nonatomic, retain) IBOutlet UIImageView *target5ImageX;
@property (nonatomic, retain) IBOutlet UIImageView *target6ImageX;
@property (nonatomic, retain) IBOutlet UIImageView *target7ImageX;

//@property (nonatomic, retain) Facebook* faceBookR;

- (IBAction)homeTapped:(id)sender;
- (IBAction)acceleratorDown:(id)sender;
- (IBAction)acceleratorUp:(id)sender;
- (IBAction)brakeDown:(id)sender;
- (IBAction)brakeUp:(id)sender;
- (void) launchCutSceenPlayer:(int)index;
- (int) currentTargetSelectNum;
- (NSString*) getImageName:(int)Num;
- (void) facebookLogin;
//- (void) gameEnd;
//- (void) levelViewControllerToBack;
//- (IBAction)levelViewControllerToBack:(id)sender;
- (void) levelViewControllerToBack;
- (void) launchSecuensesCutSceen;
- (void)challenge;
@end
