//
//  CBus.h
//  Bus Rage
//
//  Created by Jin Tie on 5/3/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CBus : CCSprite {
    CCSprite *m_BusSprite;
    CCAnimate*      m_animRun;
    CCAnimate*      m_animLeft;    
    CCAnimate*      m_animRight;
    //CCAnimate*      m_animLeftCollide;
    //CCAnimate*      m_animRightCollide;
    //CCAnimate*      m_animLeftFlip;
    //CCAnimate*      m_animRightFlip;
    
    bool m_bRunState;
}
@property (nonatomic, retain) CCAnimate*      m_animRun;
@property (nonatomic, retain) CCAnimate*      m_animLeft;
@property (nonatomic, retain) CCAnimate*      m_animRight;
//@property (nonatomic, retain) CCAnimate*      m_animLeftCollide;
//@property (nonatomic, retain) CCAnimate*      m_animRightCollide;
//@property (nonatomic, retain) CCAnimate*      m_animLeftFlip;
//@property (nonatomic, retain) CCAnimate*      m_animRightFlip;

@property (nonatomic, assign) bool m_bRunState;

- (void) setAnimRun;
- (void) stopAnimRun;
- (void) setAnimLeft;
- (void) setAnimRight;
//- (void) setAnimCollideLeft;
//- (void) setAnimCollideRight;
//- (void) setAnimFlipLeft;
//- (void) setAnimFlipRight;
@end
