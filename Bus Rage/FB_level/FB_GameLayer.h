//
//  FB_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "FB_HousesLayer.h"
#import "FB_FenceLayer.h"
#import "FB_RoadLayer.h"
#import "FB_TreesLayer.h"
#import "FB_HillsLayer.h"
#import "FB_Horizon1Layer.h"
#import "FB_BaseLayer.h"
@class MainLayer;

@interface FB_GameLayer : CCLayer {
    FB_HousesLayer *fb_houseLayer;
    FB_FenceLayer *fb_fenceLayer;
    FB_RoadLayer *fb_roadLayer;
    FB_TreesLayer *fb_treeLayer;
    FB_HillsLayer *fb_hillLayer;
    FB_Horizon1Layer *fb_horizon1Layer;
    FB_BaseLayer *fb_baseLayer;
    CCSprite *cloud_dustSprite;
    MainLayer * mainLayer;
    int m_timecount;
}

- (void) runMove: (int)runDistance speed:(int)speed;
- (void) setMainLayer:(MainLayer*) mainLayer;


@end
