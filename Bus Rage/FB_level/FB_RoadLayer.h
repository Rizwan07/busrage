//
//  FB_RoadLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
@class MainLayer;

@interface FB_RoadLayer : CCLayer {
    
    CCSprite *road1Sprtie;
    CCSprite *road2Sprtie;
    CCSprite *road3Sprtie;
    CCSprite *road4Sprtie;
    CCSprite *rampSprtie;
    CCSprite *shipSprite;
    
    int current_road;
    int runCount;
    MainLayer * mainLayer;
    BOOL isStoped;
    
    
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) changeRoadSprite;
- (void) setMainLayer:(MainLayer*) mainLayer;


@end
