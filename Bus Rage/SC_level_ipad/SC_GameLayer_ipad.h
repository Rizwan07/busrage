//
//  SC_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SC_HousesLayer_ipad.h"
#import "SC_FenceLayer_ipad.h"
#import "SC_RoadLayer_ipad.h"
#import "SC_TreesLayer_ipad.h"
#import "SC_HillsLayer_ipad.h"
#import "SC_Horizon1Layer_ipad.h"
#import "SC_BaseLayer_ipad.h"

@interface SC_GameLayer_ipad : CCLayer {
    SC_HousesLayer_ipad *sc_houseLayer;
    SC_FenceLayer_ipad *sc_fenceLayer;
    SC_RoadLayer_ipad *sc_roadLayer;
    SC_TreesLayer_ipad *sc_treeLayer;
    SC_HillsLayer_ipad *sc_hillLayer;
    SC_Horizon1Layer_ipad *sc_horizon1Layer;
    SC_BaseLayer_ipad *sc_baseLayer;
    CCSprite *cloud_dustSprite;
    int m_timecount;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
