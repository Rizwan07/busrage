//
//  CS_GameLayer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

#import "OR_HousesLayer_ipad.h"
#import "OR_FenceLayer_ipad.h"
#import "OR_RoadLayer_ipad.h"
#import "OR_TreesLayer_ipad.h"
#import "OR_HillsLayer_ipad.h"
#import "OR_Horizon1Layer_ipad.h"
#import "OR_BaseLayer_ipad.h"

@interface OR_GameLayer_ipad : CCLayer {
    OR_HousesLayer_ipad *or_houseLayer;
    OR_FenceLayer_ipad *or_fenceLayer;
    OR_RoadLayer_ipad *or_roadLayer;
    OR_TreesLayer_ipad *or_treeLayer;
    OR_HillsLayer_ipad *or_hillLayer;
    OR_Horizon1Layer_ipad *or_horizon1Layer;
    OR_BaseLayer_ipad *or_baseLayer;
    
    CCSprite *cloud_dustSprite;
    int m_timecount;
}
- (void) runMove:(int)runDistance speed:(int)speed;




@end
