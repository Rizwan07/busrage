//
//  CS_GameLayer_ipad.m
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "OR_GameLayer_ipad.h"

#import "EnumConstances.h"

@implementation OR_GameLayer_ipad

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *horizon_fogSprite = [CCSprite spriteWithFile:@"horizon_fog_ipad.png"];
        [self addChild:horizon_fogSprite z:tagRoad + 1];
        horizon_fogSprite.position = ccp(size.width/2, 150*2.4);
        
        CCSprite *horizon_farSprite = [CCSprite spriteWithFile:@"Horizon_far_OR_ipad.png"];
        [self addChild:horizon_farSprite z:tagHorizonFar];
        horizon_farSprite.position = ccp(size.width/2, 240*2.4);         
        
        cloud_dustSprite = [CCSprite spriteWithFile:@"Cloud_dust_horizon_ipad.png"];
        [self addChild:cloud_dustSprite z:tagHorizonFar + 3];
        cloud_dustSprite.position = ccp(-514*2.1333333, 220*2.4);
        
        or_houseLayer = [OR_HousesLayer_ipad node];
        [self addChild:or_houseLayer z:tagShops];        
        or_fenceLayer = [OR_FenceLayer_ipad node];
        [self addChild:or_fenceLayer z:tagFence];        
        or_roadLayer = [OR_RoadLayer_ipad node];
        [self addChild:or_roadLayer z:tagRoad];        
        or_treeLayer = [OR_TreesLayer_ipad node];
        [self addChild:or_treeLayer z:tagTrees];        
        or_hillLayer = [OR_HillsLayer_ipad node];
        [self addChild:or_hillLayer z:tagHills];        
        or_horizon1Layer = [OR_Horizon1Layer_ipad node];        
        [self addChild:or_horizon1Layer z:tagHorizon1];        
        or_baseLayer = [OR_BaseLayer_ipad node];
        [self addChild:or_baseLayer z:tagBase];
        
        [self schedule:@selector(renderCloud:) interval: 10.0];
        [self schedule:@selector(renderBalloon:) interval: 70.0];  
        [self schedule:@selector(updateGamePlay:)];
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed;
{
    [or_houseLayer runMove:runDistance speed:speed];
    [or_fenceLayer runMove:runDistance speed:speed];
    [or_roadLayer runMove:runDistance/4 speed:speed];
    [or_treeLayer runMove:runDistance speed:speed];
    [or_hillLayer runMove:runDistance speed:speed];
    [or_horizon1Layer runMove:runDistance speed:speed];
    [or_baseLayer runMove:runDistance speed:speed];
}
- (void) renderCloud: (ccTime)dt {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    int tagNum =  (arc4random()%2+1);
	NSString *strCloud = [NSString stringWithFormat:@"Cloud_%i_OR.png",tagNum];
    
    CCSprite *cloud = [CCSprite spriteWithFile:strCloud];
    cloud.scale = 0.4 + 0.01 * (arc4random()%10);
	[self addChild:cloud z:tagHorizonFar+tagNum];
    
	cloud.position = ccp((-cloud.contentSize.width/2 * cloud.scale)*2.1333333, (250 + (arc4random()% 4) * 20)*2.4);
	
	int duration = 30 + arc4random() % 20;
	id actionMove = [CCMoveTo actionWithDuration:duration position:ccp(winSize.width + cloud.contentSize.width/2 * cloud.scale, cloud.position.y)];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(cloudMoveDone:)];
	[cloud runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
} 
- (void) cloudMoveDone: (id) sender {
	CCSprite *cloud = (CCSprite *)sender;
	[self removeChild: cloud cleanup: YES];
}
- (void) renderBalloon: (ccTime)dt {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
	CCSprite *balloon = [CCSprite spriteWithFile:@"hot balloon_CS_ipad.png"];
    balloon.scale = 0.3 + 0.01 * (arc4random()%3);
    balloon.scale = balloon.scale * 4;
	[self addChild:balloon z:tagHorizonFar+4];
    
	balloon.position = ccp((-balloon.contentSize.width/2 * balloon.scale)*2.1333333, (220 + (arc4random()% 2) * 50)*2.4);
	
	int duration = 60 + arc4random() % 20;
	id actionMove = [CCMoveTo actionWithDuration:duration position:ccp(winSize.width + balloon.contentSize.width/2 * balloon.scale, balloon.position.y)];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(balloonMoveDone:)];
	[balloon runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
} 
- (void) balloonMoveDone: (id) sender {
	CCSprite *balloon = (CCSprite *)sender;
	[self removeChild:balloon cleanup: YES];
}
- (void) renderCloud_Dust: (ccTime)dt {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    int tagNum =  3;
	NSString *strCloud = @"Cloud_dust_horizon_ipad.png";
	CCSprite *cloud = [CCSprite spriteWithFile:strCloud];
	[self addChild:cloud z:tagHorizonFar+tagNum];
    
	cloud.position = ccp((-cloud.contentSize.width/2 + 480)*2.1333333, 220*2.4);
	
	int duration = 33 + arc4random() % 20;
	id actionMove = [CCMoveTo actionWithDuration:duration position:ccp(winSize.width + cloud.contentSize.width/2 * cloud.scale, cloud.position.y)];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(cloud_DustMoveDone:)];
	[cloud runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
} 
- (void) cloud_DustMoveDone: (id) sender {
	CCSprite *cloudDust = (CCSprite *)sender;
	[self removeChild:cloudDust cleanup: YES];
}

- (void)updateGamePlay:(ccTime)dt
{
    m_timecount++;
    if (m_timecount > 40000)
        m_timecount = 0;      
    
    if (m_timecount % 2 == 0) {//cloud_dust move
        
        CGPoint pos = cloud_dustSprite.position;
        pos.x += 1;
        if (pos.x > 994*2.1333333)
            pos.x = -514*2.1333333;
        cloud_dustSprite.position = pos;
    }
}

@end
