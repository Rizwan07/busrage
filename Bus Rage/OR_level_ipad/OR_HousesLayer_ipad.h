//
//  CS_HousesLayer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

@interface OR_HousesLayer_ipad : CCLayer {
    CCSprite *saloonSprite;
    CCSprite *houseSprite1;
    CCSprite *houseSprite2;
    CCSprite *cabinSprite;
    CCSprite *bushesUSprite;    
    CCSprite *billBoard1Sprite;
    CCSprite *cactus1Sprite;
    CCSprite *cactus2Sprite;
    CCSprite *plantSprite;
    CCSprite *strawSprite;
    
    int m_Saloon[3];
    int m_House1[3];
    int m_House2[3];
    int m_Cabin[3];
    int m_Bush[3];
    int m_BillBoard[3];;
    int m_Cactus1[3];
    int m_Cactus2[3];
    int m_Plant[3];
    int m_Straw[3];
    
    NSArray*    m_Objects;   
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) showHouse1:(int)runDistance;
- (void) showHouse2:(int)runDistance;
- (void) showSaloon:(int)runDistance;
- (void) showCabin:(int)runDistance;
- (void) showBush:(int)runDistance;
- (void) showBillBoard:(int)runDistance;
- (void) showCactus1:(int)runDistance;
- (void) showCactus2:(int)runDistance;
- (void) showPlant:(int)runDistance;
- (void) showStraw:(int)runDistance;

@end
