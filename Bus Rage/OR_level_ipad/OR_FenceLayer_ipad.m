//
//  CS_FenceLayer_ipad.m
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "OR_FenceLayer_ipad.h"
#import "EnumConstances.h"

#define SHOW_LIMIT 300

@implementation OR_FenceLayer_ipad

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        roadFence1 = [CCSprite spriteWithFile:@"fence_roadside_OR_01_ipad.png"];
        [self addChild:roadFence1 z:1];   
        roadFence2 = [CCSprite spriteWithFile:@"fence_roadside_OR_02_ipad.png"];
        [self addChild:roadFence2 z:1]; 
        roadFence3 = [CCSprite spriteWithFile:@"fence_roadside_OR_03_ipad.png"];
        [self addChild:roadFence3 z:1]; 
        roadFence4 = [CCSprite spriteWithFile:@"fence_roadside_OR_04_ipad.png"];
        [self addChild:roadFence4 z:1]; 
        roadFence1.position = roadFence2.position = roadFence3.position = roadFence4.position = ccp(size.width/2, 160*2.4);
        roadFence1.visible = roadFence2.visible = roadFence3.visible = roadFence4.visible = NO;
        roadFence1.visible = YES;
        
        current_fence = 1;
        runCount = 0;
        
        m_Objects = [NSArray arrayWithObjects:                   
                     @"10, 10",//bush1
                     @"11, 170",//bush2
                     @"6, 250",//board
                     @"10, 650",//bush1
                     @"11, 800",//bush2 
                     @"6, 750",//board
                     nil];
        int fence1_index_r = 0;
        int fence2_index_r = 0;
        int board_index = 0;
        for (int i = 0; i < [m_Objects count]; i++ )
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case F_OINFO_FENCE1_R:                
                    m_Bush1_R[fence1_index_r] = [[params objectAtIndex:1] intValue];
                    fence1_index_r++;                
                    break;
                case F_OINFO_FENCE2_R:                
                    m_Bush2_R[fence2_index_r] = [[params objectAtIndex:1] intValue];
                    fence2_index_r++;                
                    break;
                case F_OINFO_ROADSIGN:                
                    m_Board[board_index] = [[params objectAtIndex:1] intValue];
                    board_index++;                
                    break;
            }
        }       
        
        bush1Sprite_R = [CCSprite spriteWithFile:@"Bush_OR_ipad.png"];
        [self addChild:bush1Sprite_R z:0];        
        bush1Sprite_R.visible = NO;
        
        bush2Sprite_R = [CCSprite spriteWithFile:@"Bush_OR_ipad.png"];
        [self addChild:bush2Sprite_R z:0];        
        bush2Sprite_R.visible = NO;
        
        boardSprite = [CCSprite spriteWithFile:@"Boarding_OR_ipad.png"];
        [self addChild:boardSprite z:0];        
        boardSprite.visible = NO;	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    [self showFence:speed];
    [self showBushes1_R:runDistance];
    [self showBushes2_R:runDistance];
    [self showBoard:runDistance];
}
- (void) changeFenceSprite
{
    current_fence++;
    if (current_fence == 5)
        current_fence = 1;
    roadFence1.visible = roadFence2.visible = roadFence3.visible = roadFence4.visible = NO;
    switch (current_fence) {
        case 1:
            roadFence1.visible = YES;
            break;
        case 2:
            roadFence2.visible = YES;
            break;
        case 3:
            roadFence3.visible = YES;
            break;
        case 4:
            roadFence4.visible = YES;
            break;
        default:
            break;
    }
}

- (void) showFence:(int)speed
{  
    runCount++;
    if (runCount > 40000)
        runCount = 0;
    
    if (speed == 0)
        return;
    
    if (speed == 10) {
        [self changeFenceSprite];
    }
    if (speed == 8) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }
    if (speed == 6) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }   
    if (speed == 4) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }
    if (speed == 2) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }
    
}
- (void) showBushes1_R:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush1_R[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush1Sprite_R.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush1Sprite_R.visible = YES;
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;      
        }
        CGPoint cur_pos = bush1Sprite_R.position;
        cur_pos.x = 475*2.1333333;
        cur_pos.y = sum1*2.4;
        
        bush1Sprite_R.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(475.0*2.1333333, 0), CGPointMake(475.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(475.0*2.1333333, 148.0*2.4));//b
        bush1Sprite_R.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1;
    }
}
- (void) showBushes2_R:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush2_R[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush2Sprite_R.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush2Sprite_R.visible = YES;
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;      
        }
        CGPoint cur_pos = bush2Sprite_R.position;
        cur_pos.x = 475*2.1333333;
        cur_pos.y = sum1*2.4;
        
        bush2Sprite_R.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(475.0*2.1333333, 0), CGPointMake(475.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(475.0*2.1333333, 148.0*2.4));//b
        bush2Sprite_R.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1;
    }
}
- (void) showBoard:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Board[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    boardSprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        boardSprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        boardSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0*2.1333333, 0), CGPointMake(405.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405.0*2.1333333, 148.0*2.4));//b
        boardSprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1;
        boardSprite.scale = boardSprite.scale * 1.5;
    }
}

@end
