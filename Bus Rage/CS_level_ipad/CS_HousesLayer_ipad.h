//
//  CS_HousesLayer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

@interface CS_HousesLayer_ipad : CCLayer {
    CCSprite *houseSprite1;
    CCSprite *houseSprite2;
    CCSprite *barnSprite;
    CCSprite *farmSprite;
    CCSprite *bushesUSprite;
    CCSprite *billBoard1Sprite, *billBoard2Sprite;
    CCSprite *farmWithFenceSprite;
    CCSprite *cowSprite;
    CCSprite *cabinSprite;
    CCSprite *ditch1Sprite, *ditch2Sprite;
    CCSprite *woodenPostSprite;
    NSArray*    m_Objects;
    int m_House1[3];
    int m_House2[3];
    int m_Barn[3];
    int m_Farm[3];
    int m_Bush_U[3];
    int m_BillBoard1[3], m_BillBoard2[3];
    int m_FarmWithFence[3];
    int m_Cow[3];
    int m_Cabin[3];
    int m_Ditch1[3];
    int m_Ditch2[3];
    int m_WoodenPost[3];
}
- (void) runMove:(int)runDistance;
- (void) showHouse1:(int)runDistance;
- (void) showHouse2:(int)runDistance;
- (void) showBarn:(int)runDistance;
- (void) showFarm:(int)runDistance;
- (void) showBushes_U:(int)runDistance;
- (void) showBillBoard1:(int)runDistance;
- (void) showBillBoard2:(int)runDistance;
- (void) showFarmWithFence:(int)runDistance;
- (void) showCow:(int)runDistance;
- (void) showCabin:(int)runDistance;
- (void) showDitch1:(int)runDistance;
- (void) showDitch2:(int)runDistance;
- (void) showWoodenPost:(int)runDistance;

@end
