//
//  CA_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CA_HousesLayer_ipad.h"
#import "CA_FenceLayer_ipad.h"
#import "CA_RoadLayer_ipad.h"
#import "CA_TreesLayer_ipad.h"
#import "CA_HillsLayer_ipad.h"
#import "CA_Horizon1Layer_ipad.h"
#import "CA_BaseLayer_ipad.h"
@class MainLayer_iPad;

@interface CA_GameLayer_ipad : CCLayer {
    CA_HousesLayer_ipad *ca_houseLayer;
    CA_FenceLayer_ipad *ca_fenceLayer;
    CA_RoadLayer_ipad *ca_roadLayer;
    CA_TreesLayer_ipad *ca_treeLayer;
    CA_HillsLayer_ipad *ca_hillLayer;
    CA_Horizon1Layer_ipad *ca_horizon1Layer;
    CA_BaseLayer_ipad *ca_baseLayer;
    CCSprite *cloud_dustSprite;
    MainLayer_iPad * mainLayer;
    int m_timecount;
    
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) setMainLayer:(MainLayer_iPad*) mainLayer;
@end
