//
//  SC_HousesLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import "SC_HousesLayer.h"
#import "MainLayer.h"
#define SHOW_LIMIT 170

@implementation SC_HousesLayer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        m_Objects = [NSArray arrayWithObjects://SC:0,1, 18,19,20, 21
                     
                     @"18, 40",//school1
                     @"19, 90",//school2
                     @"20, 140",//school3
                     @"21, 240",//basketball
                     @"18, 310",//school1
                     @"19, 360",//school2
                     @"20, 410",//school3
                     
                     @"0, 550",//house1                    
                     @"1, 600",//house2
                     @"18, 700",//school1
                     @"19, 760",//school2
                     @"20, 820",//school3 
                     @"21, 920",//basketball
                     @"18, 1000",//school1
                     @"19, 1060",//school2
                     @"20, 1120",//school3 
                     nil];
        
        int house1_index = 0;
        int house2_index = 0;
        int school1_index = 0;
        int school2_index = 0;
        int school3_index = 0;
        int basketball_index = 0;
        for (int i = 0; i < [m_Objects count]; i++ )
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case H_OINFO_HOUSE1:                
                    m_House1[house1_index] = [[params objectAtIndex:1] intValue];
                    house1_index++;             
                    break;	
                case H_OINFO_HOUSE2:                
                    m_House2[house2_index] = [[params objectAtIndex:1] intValue];
                    house2_index++;                
                    break;
                case H_OINFO_SCHOOL1:
                    m_School1[school1_index] = [[params objectAtIndex:1] intValue];
                    school1_index++;
                    break;
                case H_OINFO_SCHOOL2:
                    m_School2[school2_index] = [[params objectAtIndex:1] intValue];
                    school2_index++;
                    break;
                case H_OINFO_SCHOOL3:
                    m_School3[school3_index] = [[params objectAtIndex:1] intValue];
                    school3_index++;
                    break;
                case H_OINFO_BASKETBALL:                
                    m_Basketball[basketball_index] = [[params objectAtIndex:1] intValue];
                    basketball_index++;                
                    break;	                
            }
        }
        
        house1Sprite = [CCSprite spriteWithFile:@"home_1_SV.png"];
        [self addChild:house1Sprite z:2];
        house1Sprite.visible = NO;  
        
        house2Sprite = [CCSprite spriteWithFile:@"house_2_SV.png"];
        [self addChild:house2Sprite z:1];
        house2Sprite.visible = NO;
        
        school1Sprite = [CCSprite spriteWithFile:@"school_1_SC.png"];
        [self addChild:school1Sprite z:3];
        school1Sprite.visible = NO;
        
        NSLog(@"school sprite %@", school1Sprite);
        
        school2Sprite = [CCSprite spriteWithFile:@"school_2_SC.png"];
        [self addChild:school2Sprite z:2];
        school2Sprite.visible = NO;
        
        school3Sprite = [CCSprite spriteWithFile:@"school_3_SC.png"];
        [self addChild:school3Sprite z:1];
        school3Sprite.visible = NO;
        
        basketballSprite = [CCSprite spriteWithFile:@"basketball_Court_SC.png"];
        [self addChild:basketballSprite z:0];
        basketballSprite.visible = NO;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{    
    [self showBasketball:runDistance];
    [self showHouse1:runDistance];
    [self showHouse2:runDistance];
    [self showSchool1:runDistance];
    [self showSchool2:runDistance];
    [self showSchool3:runDistance];
}
- (void) showSchool1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_School1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    school1Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        school1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        school1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        school1Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        school1Sprite.scale = school1Sprite.scale * 4;
    }
}
- (void) showSchool2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_School2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    school2Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        school2Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        school2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        school2Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        school2Sprite.scale = school2Sprite.scale * 4;
    }
}
- (void) showSchool3:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_School3[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    school3Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        school3Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        school3Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        school3Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        school3Sprite.scale = school3Sprite.scale * 4;
    }
}
- (void) showHouse1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_House1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    house1Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        house1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        house1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        house1Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        house1Sprite.scale = house1Sprite.scale * 2.0;
    }
}
- (void) showHouse2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_House2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    house2Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        house2Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        house2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        house2Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        house2Sprite.scale = house2Sprite.scale * 2.0;
    }
}
- (void) showBasketball:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Basketball[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    basketballSprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        basketballSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        basketballSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        basketballSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        basketballSprite.scale = basketballSprite.scale * 4.0;
    }
}
@end
