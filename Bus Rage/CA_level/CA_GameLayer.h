//
//  CA_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CA_HousesLayer.h"
#import "CA_FenceLayer.h"
#import "CA_RoadLayer.h"
#import "CA_TreesLayer.h"
#import "CA_HillsLayer.h"
#import "CA_Horizon1Layer.h"
#import "CA_BaseLayer.h"
@class MainLayer;

@interface CA_GameLayer : CCLayer {
    CA_HousesLayer *ca_houseLayer;
    CA_FenceLayer *ca_fenceLayer;
    CA_RoadLayer *ca_roadLayer;
    CA_TreesLayer *ca_treeLayer;
    CA_HillsLayer *ca_hillLayer;
    CA_Horizon1Layer *ca_horizon1Layer;
    CA_BaseLayer *ca_baseLayer;
    CCSprite *cloud_dustSprite;
    MainLayer * mainLayer;
    int m_timecount;
    
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) setMainLayer:(MainLayer*) mainLayer;
@end
