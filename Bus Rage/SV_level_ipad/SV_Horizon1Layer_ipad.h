//
//  SV_Horizon1Layer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SV_Horizon1Layer_ipad : CCLayer {
    CCSprite *horizonSprite;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
