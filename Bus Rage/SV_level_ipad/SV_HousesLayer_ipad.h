//
//  SV_HousesLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SV_HousesLayer_ipad : CCLayer {
    CCSprite *shop1Sprite;
    CCSprite *shop2Sprite;
    CCSprite *shop3Sprite;
    CCSprite *house1Sprite;
    CCSprite *house2Sprite; 
    CCSprite *parkSprite;
    
    int m_Shop1[4];
    int m_Shop2[4];
    int m_Shop3[4];
    int m_House1[3];
    int m_House2[3];
    int m_park[3];
    
    NSArray*    m_Objects; 
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) showShop1:(int)runDistance;
- (void) showShop2:(int)runDistance;
- (void) showShop3:(int)runDistance;
- (void) showHouse1:(int)runDistance;
- (void) showHouse2:(int)runDistance;
- (void) showPark:(int)runDistance;
@end
