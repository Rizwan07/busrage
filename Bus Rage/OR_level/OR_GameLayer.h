//
//  OR_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "OR_HousesLayer.h"
#import "OR_FenceLayer.h"
#import "OR_RoadLayer.h"
#import "OR_TreesLayer.h"
#import "OR_HillsLayer.h"
#import "OR_Horizon1Layer.h"
#import "OR_BaseLayer.h"

@interface OR_GameLayer : CCLayer {
    OR_HousesLayer *or_houseLayer;
    OR_FenceLayer *or_fenceLayer;
    OR_RoadLayer *or_roadLayer;
    OR_TreesLayer *or_treeLayer;
    OR_HillsLayer *or_hillLayer;
    OR_Horizon1Layer *or_horizon1Layer;
    OR_BaseLayer *or_baseLayer;
    CCSprite *cloud_dustSprite;    
    int m_timecount;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
