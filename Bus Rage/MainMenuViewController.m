//
//  MainMenuViewController.m
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "MainMenuViewController.h"
#import "MediaPlayer/MPMoviePlayerViewController.h"
#import "CutscenePlayer.h"

@implementation MainMenuViewController
@synthesize fbConnect;
@synthesize howtoViewController = _howtoViewController;
@synthesize selectcharacterViewController = _selectcharacterViewController;

- (BOOL) connectedToNetwork {
    // 0.0.0.0 
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
	
    // Reachability 
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
	
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
	
    if (!didRetrieveFlags)
    {
        printf("Error. Could not recover network reachability flags\n");
        return 0;
    }
	
	// 
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
	BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
	
	return ((isReachable && !needsConnection) || nonWiFi) ? YES : NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        UILabel *mainMenuLabel = [[UILabel alloc] initWithFrame:CGRectMake(112, 24, 97, 14)];
//        [mainMenuLabel setText:@"Main Menu"];
//        mainMenuLabel.font = [UIFont boldSystemFontOfSize:17];
//        [self.view addSubview:mainMenuLabel];  
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    
    [GameManager sharedGameManager].m_targetDic = [[NSMutableDictionary alloc] init];
    
    if ([GameManager sharedGameManager].m_bEnabledMusic) {
        //[CDAudioManager sharedManager].backgroundMusic.volume = 0.05f;
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"Level_music.mp3" loop:YES];        
        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    }
    
	MPMoviePlayerController* player = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [[CutscenePlayer sharedInstance] load:1 forIpad:NO];
        player = [[CutscenePlayer sharedInstance] theMovie];
        
        [player.view setFrame:CGRectMake(0.0, 0.0, 320.0, 480.0)];
        
    }
    else {
        
        [[CutscenePlayer sharedInstance] load:1 forIpad:YES];
        player = [[CutscenePlayer sharedInstance] theMovie];

        [player.view setFrame:CGRectMake(0.0, 0.0, 768.0, 1024.0)];
    }
	[self.view addSubview:player.view];		
	[[CutscenePlayer sharedInstance] play];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}
- (void) movieFinishedCallback:(NSNotification*) aNotification
{
    MPMoviePlayerController *player = [aNotification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    [player stop];   
    [player.view removeFromSuperview];
//    [player release];
}
- (void)viewDidUnload
{
    [viewCreditInformation release];
    viewCreditInformation = nil;
    [imageViewCreditInformation release];
    imageViewCreditInformation = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) viewWillAppear:(BOOL)animated
{
    if ([self connectedToNetwork] == 0) {
        [GameManager sharedGameManager].bNetWork_Con = NO;        
    } else {
        [GameManager sharedGameManager].bNetWork_Con = YES;
    }    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}
- (IBAction)nextTapped:(id)sender {  
//    if ([GameManager sharedGameManager].m_bEnabledSound) {
//        [[SimpleAudioEngine sharedEngine] playEffect:@"button_press.mp3"];
//    }
    if (_selectcharacterViewController == nil) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            self.selectcharacterViewController = [[[SelectCharacterViewController alloc] initWithNibName:@"SelectCharacterViewController" bundle:nil] autorelease];
        }
        else {
            self.selectcharacterViewController = [[[SelectCharacterViewController alloc] initWithNibName:@"SelectCharacterViewController_iPad" bundle:nil] autorelease];
        }
    }
    [self.navigationController pushViewController:_selectcharacterViewController animated:YES];
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Next_screen_swoosh.mp3"];
    }
}

- (IBAction)howtoTapped:(id)sender {
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Next_screen_swoosh.mp3"];
    }
    if (_howtoViewController == nil) {
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        self.howtoViewController = [[[HowToViewController alloc] initWithNibName:@"HowToViewController" bundle:nil] autorelease];
        }
        else {
            self.howtoViewController = [[[HowToViewController alloc] initWithNibName:@"HowToViewController_iPad" bundle:nil] autorelease];
        }
    }
    [self.navigationController pushViewController:_howtoViewController animated:YES];
}

- (IBAction)challengeBtnPressed:(id)sender {
    [self facebookLogin];
}


-(IBAction)challengeTapped:(id)sender
{
    [self facebookLogin];
}

- (void) facebookLogin
{
    
    if (TRUE) {
    //if ([self connectedToInternet]) {
        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
//        
//        internetReachable = [Reachability reachabilityForInternetConnection];
//        [internetReachable startNotifier];
        
        fbConnect=[FBConnectSingleton retrieveSingleton];
        [fbConnect setDelegate:self];

        [fbConnect login];
        
    }
    else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Network"
                                                        message:@"Please enable your Internet"
                                                       delegate:nil cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}

- (IBAction)crossBtnPressed:(id)sender {
    
    [UIView transitionWithView:viewCreditInformation duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [viewCreditInformation setHidden:YES];
    }completion:nil];
    
}

- (IBAction)creditBtnPressed:(id)sender {
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

        [imageViewCreditInformation setImage:[UIImage imageNamed:@"credit-withBG.png"]];
    }
    else {
        [imageViewCreditInformation setImage:[UIImage imageNamed:@"credit-withBG-ipad.png"]];
    }
    [UIView transitionWithView:viewCreditInformation duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [viewCreditInformation setHidden:NO];
    }completion:nil];
    
}

- (IBAction)informationBtnPressed:(id)sender {
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

        [imageViewCreditInformation setImage:[UIImage imageNamed:@"information-withBG.png"]];
    }
    else {
        [imageViewCreditInformation setImage:[UIImage imageNamed:@"information-withBG-ipad.png"]];
    }
    [UIView transitionWithView:viewCreditInformation duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [viewCreditInformation setHidden:NO];
    }completion:nil];
}

- (IBAction)linkBtnPressed:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.facebook.com/Busrageapp"]];
}

#pragma mark -
#pragma mark FB Delegate

-(void)FBConnectSingletonRequestDidLoadWithResult:(id)result{
    NSLog(@"Resukts:%@",result);
    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"id"] forKey:@"fbUserId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://developer.avenuesocial.com/azeemsal/busrage/application/json/json.php?method=challenge&user_id=%@",[result valueForKey:@"id"]]]] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *urlResponse, NSData* returnData, NSError *error){
        
        SBJSON *json = [[SBJSON new] autorelease];
        
        NSString *returnString= [[[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *dic = [NSDictionary dictionaryWithDictionary:[json objectWithString:returnString]];
        
        
        NSLog(@"Response:%@",dic);
        
        if([[dic valueForKey:@"success"] intValue]==1)
        {
            
            [GameManager sharedGameManager].challengeId = [[dic valueForKey:@"data"]valueForKey:@"Challange_id"];
            //[[NSString alloc]initWithString:[[dic valueForKey:@"data"]valueForKey:@"Challange_id"]];
            
            NSLog(@"ChallengeID:%@",[GameManager sharedGameManager].challengeId);
            NSLog(@"Length:%d",[GameManager sharedGameManager].challengeId.length);
            
            [self nextTapped:UIButtonTypeCustom];
            
        }
        
    }];
    
}

-(void)fbDidNotLogin:(BOOL)cancelled {
    
    NSLog(@"did not login");
    UIAlertView* alert = [[UIAlertView alloc] init];
    [alert setMessage:@"Failed to login to face book"];
	[alert addButtonWithTitle:@"Ok"];
	[alert setDelegate:self];
	[alert show];
	[alert release];
    //[ActivityView removeFromSuperview];
}

#pragma mark FaceBook releated methods END
- (void)dealloc {
    [viewCreditInformation release];
    [imageViewCreditInformation release];
    [super dealloc];
}
@end
