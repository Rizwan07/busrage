//
//  CS_RoadLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 4/29/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CS_RoadLayer : CCLayer {
    CCSprite *road1Sprite;
    CCSprite *road2Sprite;
    CCSprite *road3Sprite;
    CCSprite *road4Sprite;    
    int current_road;
    int runCount;
    
    //CCSprite *ground1Sprite;
    //CCSprite *ground2Sprite;
    //int current_ground;
}
- (void) runMove:(int)speed;
- (void) changeRoadSprite;
@end
