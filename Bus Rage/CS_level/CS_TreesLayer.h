//
//  CS_TreesLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 4/30/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CS_TreesLayer : CCLayer {
    CCSprite *tree1Sprite;
    CCSprite *tree2Sprite;
    CCSprite *windmillSprite;
    CCSprite *windmillflierSprite;
    CCSprite *bush1Sprite;
    CCSprite *bush2Sprite;
    int m_Tree1[3];
    int m_Tree2[3];
    int m_Windmill[4];
    int m_Bush1[3];
    int m_Bush2[3];
    
    NSArray*    m_Objects;    
}
- (void) runMove:(int)runDistance;
- (void) showTree1:(int)runDistance;
- (void) showTree2:(int)runDistance;
- (void) showWindmill:(int)runDistance;
- (void) showBush1:(int)runDistance;
- (void) showBush2:(int)runDistance;
@end
