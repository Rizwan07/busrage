//
//  CS_HillsLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 4/30/12.
//  Copyright 2012 k. All rights reserved.
//

#import "CS_HillsLayer.h"


@implementation CS_HillsLayer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
    
        hill1Sprite = [CCSprite spriteWithFile:@"Hill_Yellow_CS.png"];
        [self addChild:hill1Sprite z:2];        
        hill1Sprite.visible = YES;
        hill1Sprite.position = ccp(280, 144);
        //hill1Sprite.scale = 0.3;
        
        hill2Sprite = [CCSprite spriteWithFile:@"Hill_Green_stripped_CS.png"];
        [self addChild:hill2Sprite z:1];        
        hill2Sprite.visible = YES;
        hill2Sprite.position = ccp(100, 140);
        //hill2Sprite.scale = 0.3;
        
        hill3Sprite = [CCSprite spriteWithFile:@"Hill_Yellow_CS.png"];
        [self addChild:hill3Sprite z:0];        
        hill3Sprite.visible = YES;
        hill3Sprite.position = ccp(200, 148);
        //hill3Sprite.scale = 0.3;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    if (runDistance < 10) {
        hill1Sprite.position = ccp(280, 144);
        hill2Sprite.position = ccp(100, 140);
        hill3Sprite.position = ccp(200, 148);
    }
    if (runDistance > 720 && runDistance < 730) {
        hill1Sprite.position = ccp(280, 144);
        hill2Sprite.position = ccp(100, 140);
        hill3Sprite.position = ccp(200, 148);
    }
    if (speed > 0) {
        CGPoint pos;
        pos = hill1Sprite.position;
        pos.x -= 0.4 * speed/2;
        hill1Sprite.position = pos; 
        
        pos = hill2Sprite.position;
        pos.x -= 0.4 * speed/2;
        hill2Sprite.position = pos; 
        
        pos = hill3Sprite.position;
        pos.x -= 0.4 * speed/2;
        hill3Sprite.position = pos; 
    }
    
}
@end
