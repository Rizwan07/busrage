//
//  FBConnectSingleton.h
//  FortuneCookie
//
//  Created by Hassan Ahmed on 1/27/11.
//  Copyright 2011 Salsoft Technologies Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBConnect.h"

@protocol FBConnectDelegate;

@interface FBConnectSingleton : NSObject <FBRequestDelegate, FBDialogDelegate, FBSessionDelegate>{
	Facebook* _facebook;
	NSArray* _permissions;
	//NSMutableArray *friendsArray;
	BOOL isLogIn;
	id<FBConnectDelegate> delegate;
}
@property (nonatomic, assign) id<FBConnectDelegate> delegate;
@property(readonly) Facebook *facebook;
@property(nonatomic, assign) BOOL isLogIn;
+(FBConnectSingleton*)retrieveSingleton;
-(void)getUserInfo;
-(void)login;
-(void)logout;
-(void)getFriendsList;
- (void)publishStreamWithName:(NSString*) name caption:(NSString*) caption description:(NSString*) desc andHref:(NSString*)href;
- (void)publishStreamOnFriend:(NSString*)uid WithName:(NSString*) name caption:(NSString*) caption description:(NSString*) desc andHref:(NSString*)href;
- (void)publishImageStreamWithImageUrl:(NSString*)imgURL andName:(NSString*) name caption:(NSString*) caption description:(NSString*) desc andHref:(NSString*)href;
- (void)publishImageStreamWithImageUrl:(NSString*)imgURL andFriend:(NSString*)uid WithName:(NSString*) name caption:(NSString*) caption description:(NSString*) desc andHref:(NSString*)href;

- (void)requestWithGraphPathParams:(NSMutableDictionary *)params;

@end

@protocol FBConnectDelegate<NSObject>
@optional
-(void)FBConnectSingletonLoggedIn;
-(void)FBConnectSingletonRequestDidLoadWithResult:(id)result;
-(void)FBConnectSingletonFriendListRecievedWithList:(NSArray *)results;
-(void)FBConnectSingletonStreamDailogDidCompleteWithURL:(NSURL *)url;
-(void)fbDidNotLogin:(BOOL)cancelled;
@end