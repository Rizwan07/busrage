//
//  FBConnectSingleton.m
//  FortuneCookie
//
//  Created by Hassan Ahmed on 1/27/11.
//  Copyright 2011 Salsoft Technologies Pvt. Ltd. All rights reserved.
//

#import "FBConnectSingleton.h"
//static NSString* kAppId = @"181223981999087";
//static NSString* kAppId = @"256378557806343";

@implementation FBConnectSingleton
static FBConnectSingleton *sharedSingleton = nil;

@synthesize facebook = _facebook, isLogIn, delegate;

+(FBConnectSingleton*)retrieveSingleton {
	@synchronized(self) {
		if (sharedSingleton == nil) {
			sharedSingleton = [[FBConnectSingleton alloc] init];
		}
	}
	return sharedSingleton;
}

+ (id) allocWithZone:(NSZone *) zone {
	@synchronized(self) {
		if (sharedSingleton == nil) {
			sharedSingleton = [super allocWithZone:zone];
			return sharedSingleton;
		}
	}
	return nil;
}

-(void)login
{
    _permissions =  [NSArray arrayWithObjects:
                      @"read_stream", @"offline_access", @"user_birthday", @"email", @"user_location", @"user_hometown", @"friends_birthday", nil];
	_facebook = [[Facebook alloc] initWithAppId:kAppId];
	[_facebook authorize:_permissions delegate:self];
}

-(void)logout
{
	[_facebook logout:self];
    
}
- (void)fbDidLogin {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *token = _facebook.accessToken;
    NSDate *expireDate = _facebook.expirationDate;
    
    [defaults setObject:token forKey:@"ACCESS_TOKEN_KEY"];
    [defaults setObject:expireDate forKey:@"EXPIRATION_DATE_KEY"];
    [defaults synchronize];
    
	NSLog(@"Did Login. Hurrah!");
	if ([self.delegate respondsToSelector:@selector(FBConnectSingletonLoggedIn)])
	{
		[delegate FBConnectSingletonLoggedIn];
	}
	[self getUserInfo];
	isLogIn = YES;
}

- (void)publishImageStreamWithImageUrl:(NSString*)imgURL andName:(NSString*) name caption:(NSString*) caption description:(NSString*) desc andHref:(NSString*)href
{
	if ([imgURL isEqualToString:@""]) {
		
		imgURL = @"http://i0006.photobucket.com/albums/0006/findstuff22/Best Images/Photography/color1.jpg";
	}
	
	SBJSON *jsonWriter = [[SBJSON new] autorelease];
	
	NSDictionary* actionLinks = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:name,@"text", href, @"href", nil], nil];
	
	NSString *actionLinksStr = [jsonWriter stringWithObject:actionLinks];
	
	NSDictionary* imageShare = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"image", @"type",
                                imgURL, @"src",
                                href, @"href",
                                nil];
	

 NSDictionary* attachment = [NSDictionary dictionaryWithObjectsAndKeys:
								name, @"name",
								caption, @"caption",
								desc, @"description",
                             @"Get it now!",  @"message",
								href, @"href", 
								[NSArray arrayWithObject:imageShare], @"media", nil];
			
	NSString *attachmentStr = [jsonWriter stringWithObject:attachment];
	//NSLog(@"%@",attachmentStr);
	NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   kAppId, @"api_key",
								   @"Share on Facebook",  @"user_message_prompt",
								   actionLinksStr, @"action_links",
								   attachmentStr, @"attachment",
								   nil];
	
	
	[_facebook dialog:@"stream.publish"
			andParams:params
		  andDelegate:self];
	
}

- (void)publishImageStreamWithImageUrl:(NSString*)imgURL andFriend:(NSString*)uid WithName:(NSString*) name caption:(NSString*) caption description:(NSString*) desc andHref:(NSString*)href
{
	if ([imgURL isEqualToString:@""]) {
		
		imgURL = @"http://i0006.photobucket.com/albums/0006/findstuff22/Best Images/Photography/color1.jpg";
	}
	
	SBJSON *jsonWriter = [[SBJSON new] autorelease];
	
	NSDictionary* actionLinks = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:name,@"text", href, @"href", nil], nil];
	
	NSString *actionLinksStr = [jsonWriter stringWithObject:actionLinks];
	
	NSDictionary* imageShare = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"image", @"type",
                                imgURL, @"src",
                                href, @"href",
                                nil];
	
	
	NSDictionary* attachment = [NSDictionary dictionaryWithObjectsAndKeys:
								name, @"name",
								caption, @"caption",
								desc, @"description",
								href, @"href", 
								[NSArray arrayWithObject:imageShare], @"media", nil];
	
	NSString *attachmentStr = [jsonWriter stringWithObject:attachment];
	//NSLog(@"%@",attachmentStr);
	NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   uid, @"target_id",
								   @"Share on Facebook",  @"user_message_prompt",
								   actionLinksStr, @"action_links",
								   attachmentStr, @"attachment",
								   nil];
	
	
	[_facebook dialog:@"stream.publish"
			andParams:params
		  andDelegate:self];
	
}

- (void)publishStreamWithName:(NSString*) name caption:(NSString*) caption description:(NSString*) desc andHref:(NSString*)href
{
    
	SBJSON *jsonWriter = [[SBJSON new] autorelease];
	
	NSDictionary* actionLinks = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:name,@"text", href, @"href", nil], nil];
	
	NSString *actionLinksStr = [jsonWriter stringWithObject:actionLinks];
	NSDictionary* attachment = [NSDictionary dictionaryWithObjectsAndKeys:
								name, @"name",
                                href, @"link",
                                [fb_publish_image stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] , @"picture",
								caption, @"caption",
								desc, @"description",
                                @"Get it now!",  @"message",
                                actionLinksStr, @"action_links",
								//href, @"href",
                                nil];
	
	NSString *attachmentStr = [jsonWriter stringWithObject:attachment];
	NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   kAppId, @"api_key",
								   @"Share on Facebook",  @"user_message_prompt",
								   //actionLinksStr, @"action_links",
								   attachmentStr, @"attachment",
								   nil];
	
	[_facebook dialog:@"stream.publish"
			andParams:params
		  andDelegate:self];
}

- (void)publishStreamOnFriend:(NSString*)uid WithName:(NSString*) name caption:(NSString*) caption description:(NSString*) desc andHref:(NSString*)href
{
	
	SBJSON *jsonWriter = [[SBJSON new] autorelease];
	
		NSDictionary* actionLinks = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:name,@"text", href, @"href", nil], nil];
	
	NSString *actionLinksStr = [jsonWriter stringWithObject:actionLinks];
	NSDictionary* attachment = [NSDictionary dictionaryWithObjectsAndKeys:
								name, @"name",
								caption, @"caption",
								desc, @"description",
								href, @"href", nil];
    
	NSString *attachmentStr = [jsonWriter stringWithObject:attachment];
	NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   uid, @"target_id",
								   @"Share on Facebook",  @"user_message_prompt",
								   actionLinksStr, @"action_links",
								   attachmentStr, @"attachment",
								   nil];
	
	
	[_facebook dialog:@"stream.publish"
			andParams:params
		  andDelegate:self];
}

-(void)getFriendsList
{
	[_facebook requestWithGraphPath:@"me/friends?fields=id,name,birthday,picture,gender,email" andDelegate:self];
}

-(void)getUserInfo
{
	[_facebook requestWithGraphPath:@"me" andDelegate:self];
}

- (void)requestWithGraphPathParams:(NSMutableDictionary *)params {
    
    [_facebook requestWithGraphPath:@"me/feed" andDelegate:self];
    
//    NSMutableDictionary *params1 = [NSMutableDictionary dictionaryWithObjectsAndKeys: @"How are you", @"message",nil];
//    [_facebook requestWithGraphPath:@"me/feed" andParams:params1 andHttpMethod:@"POST" andDelegate:self];
    
    //[_facebook requestWithGraphPath:@"me/feed" andParams:params andHttpMethod:@"POST" andDelegate:self]; //fb here is the FaceBook instance.
}

/**
 * Called when the user canceled the authorization dialog.
 */
-(void)fbDidNotLogin:(BOOL)cancelled {
	NSLog(@"did not login");
	isLogIn = NO;
    
    if ([self.delegate respondsToSelector:@selector(fbDidNotLogin:)]) {
        [delegate fbDidNotLogin:cancelled];
    }
}

/**
 * Called when the request logout has succeeded.
 */
- (void)fbDidLogout {
	NSLog(@"Did Logout. Yahoo!");
	isLogIn = NO;
	[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FBLogin"];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
// FBRequestDelegate

/**
 * Called when the Facebook API request has returned a response. This callback
 * gives you access to the raw response. It's called before
 * (void)request:(FBRequest *)request didLoad:(id)result,
 * which is passed the parsed response object.
 */
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
	NSLog(@"received response");
};

/**
 * Called when a request returns and its response has been parsed into an object.
 * The resulting object may be a dictionary, an array, a string, or a number, depending
 * on the format of the API response.
 * If you need access to the raw response, use
 * (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response.
 */
- (void)request:(FBRequest *)request didLoad:(id)result {
	
    if ([request.url hasPrefix:@"https://graph.facebook.com/me/friends"]) 
	{
		if ([result isKindOfClass:[NSDictionary class]]) {
			if ([self.delegate respondsToSelector:@selector(FBConnectSingletonFriendListRecievedWithList:)])
			{
				[delegate FBConnectSingletonFriendListRecievedWithList:[result objectForKey:@"data"]];
			}
		}
	}
    
    else// if ([self.delegate respondsToSelector:@selector(FBConnectSingletonRequestDidLoadWithResult:)])
	{
		[delegate FBConnectSingletonRequestDidLoadWithResult:result];
	}

}

/**
 * Called when an error prevents the Facebook API request from completing successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
	//[self.label setText:[error localizedDescription]];
};


///////////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark FBConnectDelegate Delegate
-(void)FBConnectSingletonLoggedIn {
	//[fbSingleton publishStreamWithName:@"Couponz" caption:@"Couponz iPhone App" description:[NSString stringWithFormat:@"%@ %@ just redeemed \"%@\" at \"%@\" using Couponz.", @"Syed", @"Aqeel", @"ABC Offer", @"Business Name"] andHref:@"http://www.google.com"];

}

-(void)FBConnectSingletonRequestDidLoadWithResult:(id)result
{
	//[appDelegate getIn];
	
	
}
// FBDialogDelegate

/**
 * Called when a UIServer Dialog successfully return.
 */
- (void)dialogDidComplete:(FBDialog *)dialog {
	
	NSLog(@"dialogDidComplete called");
}
- (void)dialogCompleteWithUrl:(NSURL *)url
{
	if ([self.delegate respondsToSelector:@selector(FBConnectSingletonStreamDailogDidCompleteWithURL:)]) 
	{
		[delegate FBConnectSingletonStreamDailogDidCompleteWithURL:url];
	}
	NSLog(@"dialogCompleteWithUrl called %@", url);
}
- (void)dialogDidNotComplete:(FBDialog *)dialog
{
	NSLog(@"dialogDidNotComplete called");
}
- (void)dealloc
{
	[_facebook release];
	[super dealloc];
}
@end
