//
//  EnumConstances.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#ifndef Bus_Rage_EnumConstances_h
#define Bus_Rage_EnumConstances_h

//Layer
typedef enum _FENCE_OBJECTS_INFO_
{
    F_OINFO_FENCE1_L = 0,//Bushes1_CS.png
    F_OINFO_FENCE2_L = 1,//Bushes2_CS.png
    F_OINFO_GRASS1 = 2,
    F_OINFO_GRASS2 = 3,
    F_OINFO_FENCE3_L = 4,//Bush_1_MV.png
    F_OINFO_FENCE4_L = 5,//Bush_2_MV.png
    F_OINFO_ROADSIGN = 6,//Road_sign_MV.png
    
    F_OINFO_FENCE1_R = 10,//Bushes1_CS.png
    F_OINFO_FENCE2_R = 11,//Bushes2_CS.png
    F_OINFO_FENCE3_R = 12,//Bush_1_MV.png
    F_OINFO_FENCE4_R = 13,//Bush_2_MV.png
    
    //add SC:14,15,16,17,18,6,0(tree1),1(tree2)
    F_OINFO_LIGHT1 = 14,
    F_OINFO_LIGHT2 = 15,
    F_OINFO_LIGHT3 = 16,
    F_OINFO_LIGHT4 = 17,
    F_OINFO_HOARD = 18,
    //add SV:
    F_OINFO_FIREHYDRANT = 19,    
}FENCE_OBJECTS_INFO;

typedef enum _HOUSE_OBJECTS_INFO_
{
    //CS:0,1,2,3,4,5,6,7,8,9,10,11,12
    H_OINFO_HOUSE1 = 0,
    H_OINFO_HOUSE2 = 1,
    H_OINFO_BARN = 2,
    H_OINFO_FARM = 3,
    H_OINFO_BUSHES_U = 4,
    H_OINFO_BILLBOARD1 = 5,
    H_OINFO_BILLBOARD2 = 6,
    H_OINFO_FARM_FENCE = 7,
    H_OINFO_COW = 8,
    H_OINFO_CABIN = 9,
    H_OINFO_DITCH1 = 10,
    H_OINFO_DITCH2 = 11,
    H_OINFO_WOODENPOST = 12,
    //MV:0,1,5,6,10,11,13,14,15,16,17
    H_OINFO_GASSTATION = 13,
    H_OINFO_TREE1 = 14,
    H_OINFO_TREE2 = 15,
    H_OINFO_ROCK1 = 16,
    H_OINFO_ROCK2 = 17,
    //OR:0,1,2,9,4,5,14,15,16(plant),17(straw)
    //SC:0,1,18,19,20,21
    H_OINFO_SCHOOL1 = 18,
    H_OINFO_SCHOOL2 = 19,
    H_OINFO_SCHOOL3 = 20,
    H_OINFO_BASKETBALL = 21,
    //SA:22
    H_OINFO_STOREATTACK = 22, 
}HOUSE_OBJECTS_INFO;

typedef enum _TREE_OBJECTS_INFO_
{
    T_OINFO_TREE1 = 0,
    T_OINFO_TREE2 = 1,
    T_OINFO_WINDMILL = 2,
    T_OINFO_BUSH1 = 3,
    T_OINFO_BUSH2 = 4,
    //add MV:0,1,3,4,5,6
    T_OINFO_ROCK1 = 5,
    T_OINFO_ROCK2 = 6,
    
    //add SC
    T_OINFO_HOUSE1 = 7,
    T_OINFO_HOUSE2 = 8,
    T_OINFO_SCHOOL1 = 9,
    T_OINFO_SCHOOL2 = 10,
    T_OINFO_SCHOOL3 = 11,
}TREE_OBJECTS_INFO;

//
enum _LAYER_TAG_ {
    tagBase     = 0,
    tagHorizonFar = 10,
    tagHorizon1 = 20,//(1 ~ 2)    
    tagRoad     = 30,    
    tagHills    = 40,
    tagTrees    = 50,
    tagShops    = 60, 
    tagFence    = 70,
    tagTarget   = 80,
    tagBus      = 120,
    
}LAYER_TAG;

enum _CAR_DIRECTION_ {
    D_STRAIGHT = 0,
    D_LEFT = 1,
    D_RIGHT = 2,   
}CAR_DIRECTION;


#endif
