//
//  CTarget.h
//  Bus Rage
//
//  Created by Jin Tie on 5/17/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CTarget : CCSprite {    
    
    CCSprite *m_Sprite;
    int m_selectNum;//1...29
    int m_distance;//0...800, distance between bus and target
    //bool m_bLeftSide;
    int m_road_width;//0....6
    bool m_bBusHit;
    
    CCAnimate*      m_animWalk01;
    CCAnimate*      m_animWalk02;
    CCAnimate*      m_animWalk03;
    CCAnimate*      m_animWalk04;
    CCAnimate*      m_animWalk05;
    CCAnimate*      m_animWalk06;
    CCAnimate*      m_animWalk07;
    CCAnimate*      m_animWalk08;
    CCAnimate*      m_animWalk09;
    CCAnimate*      m_animWalk10;
    CCAnimate*      m_animWalk11;
    CCAnimate*      m_animWalk12;
    CCAnimate*      m_animWalk13;
    CCAnimate*      m_animWalk14;
    CCAnimate*      m_animWalk15;
    CCAnimate*      m_animWalk16;
    CCAnimate*      m_animWalk17;
    CCAnimate*      m_animWalk18;
    CCAnimate*      m_animWalk19;
    CCAnimate*      m_animWalk20;
    CCAnimate*      m_animWalk21;
    CCAnimate*      m_animWalk22;
    CCAnimate*      m_animWalk23;
    CCAnimate*      m_animWalk24;
    CCAnimate*      m_animWalk25;
    CCAnimate*      m_animWalk26;
    CCAnimate*      m_animWalk27;
    CCAnimate*      m_animWalk28;
    CCAnimate*      m_animWalk29;
    
    CCAnimate*      m_animIdle01;
    CCAnimate*      m_animIdle02;
    CCAnimate*      m_animIdle03;
    CCAnimate*      m_animIdle04;
    CCAnimate*      m_animIdle05;
    CCAnimate*      m_animIdle06;
    CCAnimate*      m_animIdle07;
    CCAnimate*      m_animIdle08;
    CCAnimate*      m_animIdle09;
    CCAnimate*      m_animIdle10;
    CCAnimate*      m_animIdle11;
    CCAnimate*      m_animIdle12;
    CCAnimate*      m_animIdle13;
    CCAnimate*      m_animIdle14;
    CCAnimate*      m_animIdle15;
    CCAnimate*      m_animIdle16;
    CCAnimate*      m_animIdle17;
    CCAnimate*      m_animIdle18;
    CCAnimate*      m_animIdle19;
    CCAnimate*      m_animIdle20;
    CCAnimate*      m_animIdle21;
    CCAnimate*      m_animIdle22;
    CCAnimate*      m_animIdle23;
    CCAnimate*      m_animIdle24;
    CCAnimate*      m_animIdle25;
    CCAnimate*      m_animIdle26;
    CCAnimate*      m_animIdle27;
    CCAnimate*      m_animIdle28;
    CCAnimate*      m_animIdle29;    
}
@property (assign) int m_selectNum;
@property (assign) int m_distance;
//@property (assign) bool m_bLeftSide;
@property (assign) int m_road_width;
@property (assign) bool m_bBusHit;//input at bus 
@property (nonatomic, retain) CCSprite *m_Sprite;
- (void) setAnimWalk;
- (void) setAnimIdle;
@end
