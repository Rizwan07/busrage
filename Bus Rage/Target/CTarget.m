//
//  CTarget.m
//  Bus Rage
//
//  Created by Jin Tie on 5/17/12.
//  Copyright 2012 k. All rights reserved.
//

#import "CTarget.h"


@implementation CTarget
@synthesize m_selectNum;
@synthesize m_distance;
//@synthesize m_bLeftSide;
@synthesize m_road_width;
@synthesize m_Sprite;
@synthesize m_bBusHit;
- (id) init {
    CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
    NSMutableArray *frames = [NSMutableArray array];
    
    CCSpriteFrame *frame;
    
    self = [super init];
    if (self) {
        float fdelay = 0.2f;
        m_bBusHit = NO;
        
        m_Sprite = [CCSprite spriteWithSpriteFrameName:@"African_standing.png"];//default        
        [self addChild:m_Sprite z:0];
        
        [frames removeAllObjects];  
        frame = [frameCache spriteFrameByName:@"latin_female_01.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"latin_female_02.png"];
        [frames addObject:frame];
		m_animWalk01 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];  
        frame = [frameCache spriteFrameByName:@"Mexican_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"Mexican_right_step.png"];
        [frames addObject:frame];
		m_animWalk02 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];  
        frame = [frameCache spriteFrameByName:@"female_american_01.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"female_american_02.png"];
        [frames addObject:frame];
		m_animWalk03 = m_animWalk04 = m_animWalk05 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"American_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"American_right_step.png"];
        [frames addObject:frame];
		m_animWalk06 = m_animWalk07 = m_animWalk08 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"female_base_01.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"female_base_02.png"];
        [frames addObject:frame];
		m_animWalk09 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"Arab_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"Arab_right_step.png"];
        [frames addObject:frame];
		m_animWalk10 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"asian_female_01.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"asian_female_02.png"];
        [frames addObject:frame];
		m_animWalk11 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"Asian_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"Asian_right_step.png"];
        [frames addObject:frame];
		m_animWalk12 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"german_female_01.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"german_female_02.png"];
        [frames addObject:frame];
		m_animWalk13 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"German_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"German_right_step.png"];
        [frames addObject:frame];
		m_animWalk14 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"italian_female_01.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"italian_female_02.png"];
        [frames addObject:frame];
		m_animWalk15 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"italian_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"italian_right_step.png"];
        [frames addObject:frame];
		m_animWalk16 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"african_female_01.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"african_female_02.png"];
        [frames addObject:frame];
		m_animWalk17 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"African_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"African_right_step.png"];
        [frames addObject:frame];
		m_animWalk18 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"russian_female_01.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"russian_female_02.png"];
        [frames addObject:frame];
		m_animWalk19 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"Russian_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"Russian_right_step.png"];
        [frames addObject:frame];
		m_animWalk20 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]]; 
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"nurse_female_01.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"nurse_female_02.png"];
        [frames addObject:frame];
		m_animWalk21 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"Nurse_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"Nurse_right_step.png"];
        [frames addObject:frame];
		m_animWalk22 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"woman_in_suit_01.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"woman_in_suit_02.png"];
        [frames addObject:frame];
		m_animWalk23 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"MIS_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"MIS_right_step.png"];
        [frames addObject:frame];
		m_animWalk24 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"basketball_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"basketball_right_step.png"];
        [frames addObject:frame];
		m_animWalk25 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"Coverall_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"Coverall_right_step.png"];
        [frames addObject:frame];
		m_animWalk26 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"doctor_female_01.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"doctor_female_02.png"];
        [frames addObject:frame];
		m_animWalk27 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"Doctor_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"Doctor_right_step.png"];
        [frames addObject:frame];
		m_animWalk28 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];
        
        [frames removeAllObjects];
        frame = [frameCache spriteFrameByName:@"Football_left_step.png"];
        [frames addObject:frame];
        frame = [frameCache spriteFrameByName:@"Football_right_step.png"];
        [frames addObject:frame];
		m_animWalk29 = [CCAnimate actionWithAnimation:[CCAnimation animationWithFrames:frames delay:fdelay]];        
        [frames removeAllObjects];
        m_road_width = rand() % 7;
    }
    return self;
}
- (void) dealloc
{       
    [super dealloc];
}
- (void) setAnimWalk
{
    [m_Sprite stopAllActions];
    switch (m_selectNum) {
        case 1:      
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk01]];
            break;
        case 2:  
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk02]];
            break;  
        case 3: 
        case 4: 
        case 5:      
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk05]];
            break;
        case 6:   
        case 7: 
        case 8: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk08]];
            break;
        case 9: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk09]];
            break;
        case 10: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk10]];
            break;
        case 11:      
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk11]];
            break;
        case 12:  
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk12]];
            break;  
        case 13: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk13]];
            break;
        case 14: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk14]];
            break;
        case 15:      
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk15]];
            break;
        case 16:  
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk16]];
            break;  
        case 17: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk17]];
            break;
        case 18: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk18]];
            break;
        case 19: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk19]];
            break;
        case 20: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk20]];
            break;
        case 21:      
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk21]];
            break;
        case 22:  
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk22]];
            break;  
        case 23: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk23]];
            break;
        case 24: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk24]];
            break;
        case 25:      
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk25]];
            break;
        case 26:  
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk26]];
            break;  
        case 27: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk27]];
            break;
        case 28: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk28]];
            break;
        case 29: 
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk29]];
            break;
        default:
            [m_Sprite runAction:[CCRepeatForever actionWithAction:m_animWalk01]];
            break;
    }   
}
- (void) setAnimIdle
{    
    [m_Sprite stopAllActions];  
    switch (m_selectNum) {
        case 1:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"latin_female_03.png"]];
            break;  
        case 2:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"Mexican_standing.png"]];
            break;
        case 3:  
        case 4:   
        case 5:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"female_american_03.png"]];
            break;
        case 6:   
        case 7:   
        case 8:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"American_standing.png"]];
            break; 
        case 9:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"female_base_03.png"]];
            break;
        case 10:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"Arab_standing.png"]];
            break; 
        case 11:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"asian_female_03.png"]];
            break; 
        case 12:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"Asian_standing.png"]];
            break; 
        case 13:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"german_female_03.png"]];
            break;
        case 14:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"German_Standing.png"]];
            break; 
        case 15:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"italian_female_03.png"]];
            break;
        case 16:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"italian_standing.png"]];
            break; 
        case 17:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"african_female_03.png"]];
            break;
        case 18:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"African_standing.png"]];
            break; 
        case 19:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"russian_female_03.png"]];
            break;
        case 20:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"Russian_standing.png"]];
            break;
        case 21:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"nurse_female_03.png"]];
            break; 
        case 22:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"Nurse_standing.png"]];
            break; 
        case 23:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"woman_in_suit_03.png"]];
            break;
        case 24:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"MIS_standing.png"]];
            break; 
        case 25:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"basketball_standing.png"]];
            break;
        case 26:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"Coverall_standing.png"]];
            break; 
        case 27:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"doctor_female_03.png"]];
            break;
        case 28:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"Doctor_standing.png"]];
            break; 
        case 29:        
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"Football_standing.png"]];
            break;
        default:
            [m_Sprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"latin_female_03.png"]];
            break;
    }
}
@end
