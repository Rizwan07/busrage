//
//  MainMenuViewController.h
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HowToViewController.h"
#import "SelectCharacterViewController.h"
#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "GameManager.h"
#import "SimpleAudioEngine.h"
#import "FBConnectSingleton.h"

@interface MainMenuViewController : UIViewController<FBConnectDelegate> {
    HowToViewController *_howtoViewController;
    SelectCharacterViewController *_selectcharacterViewController;
    FBConnectSingleton *fbConnect;
    IBOutlet UIView *viewCreditInformation;
    IBOutlet UIImageView *imageViewCreditInformation;
}
@property (nonatomic, retain) FBConnectSingleton *fbConnect;
@property (retain) HowToViewController *howtoViewController;
@property (retain) SelectCharacterViewController *selectcharacterViewController;
- (IBAction)nextTapped:(id)sender;
- (IBAction)howtoTapped:(id)sender;
- (IBAction)challengeBtnPressed:(id)sender;

- (void)facebookLogin;

- (IBAction)crossBtnPressed:(id)sender;
- (IBAction)creditBtnPressed:(id)sender;
- (IBAction)informationBtnPressed:(id)sender;

- (IBAction)linkBtnPressed:(id)sender;

@end
