//
//  ADBannerViewDelegate.h
//  Bus Rage
//
//  Created by Viktor on 21.11.12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BRBannerView;
@protocol BRBannerViewDelegate <NSObject>
- (void) bannerLoadded:(BRBannerView*) bannerView;
- (void) imagesLoaded: (NSArray*) images;
@end
