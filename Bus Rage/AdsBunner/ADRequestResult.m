//
//  ADRequestResult.m
//  Bus Rage
//
//  Created by Viktor on 20.11.12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "ADRequestResult.h"

@interface ADRequestResult ()
{
    
}

- (void) downloadImageByUrl:(NSURL*)anUrl;

@end

@implementation ADRequestResult

@synthesize bannerId, bannerImage, bannerState;

- (id)initWithImageUrl:(NSURL*) anUrl bannerState:(int) state bannerId:(int) Id
{
    self = [super init];
    if (self) {
        self.bannerId = Id;
        self.bannerState = state;
        [self downloadImageByUrl:anUrl];
    }
    return self;
}

- (void) downloadImageByUrl:(NSURL*) anUrl
{
    self.bannerImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:anUrl]];
}

@end
