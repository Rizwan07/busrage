//
//  ADAdapter.h
//  Bus Rage
//
//  Created by Viktor on 23.11.12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADAdapter : NSObject
+(void) insertADSToSprites:(NSArray*) sprites;
+ (void) setADSImages:(NSArray*) images;
+ (void) setIsWaitForAds:(BOOL) isNeedToWait;
+ (void) addImages:(NSArray*) images toSprites:(NSArray*) sprites;
@end
