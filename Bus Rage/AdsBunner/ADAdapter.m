//
//  ADAdapter.m
//  Bus Rage
//
//  Created by Viktor on 23.11.12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "ADAdapter.h"
#import "ADRequestResult.h"
#import "cocos2d.h"

static NSArray * adBanners;
static NSMutableArray * spritesToAddAds;
static BOOL isWaitForAds = YES;
@implementation ADAdapter

+ (void) setADSImages:(NSArray*) images
{
    if (adBanners) {
        [adBanners release];
    }
    
    adBanners = images;
    [adBanners retain];
    
    if (spritesToAddAds) {
        [self insertADSToSprites:spritesToAddAds];
        [spritesToAddAds release];
    }
}

+ (void) insertADSToSprites:(NSArray*) sprites
{
    if (!isWaitForAds) {
        return;
    }
    if (adBanners) {
        [self addImages:adBanners toSprites:sprites];
    }else if(spritesToAddAds)
    {
        [spritesToAddAds addObjectsFromArray:sprites];
    }
    else{
        spritesToAddAds = [[NSMutableArray alloc] initWithArray:sprites];
    }
}

+ (void) addImages:(NSArray*) images toSprites:(NSArray*) sprites
{
    int i = 0;
    float x = 1.0;
    float y = 1.0;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isIpad"]) {
        x = 1024.0/480.0;
        y = 768.0/320.0;
    }
    for (CCSprite * sprite in sprites) {
        if (i < images.count) {
            UIImage * image = [images objectAtIndex:i];
//            CCSprite * billboardImage = [[CCSprite alloc] initWithCGImage:image.CGImage key:[NSString stringWithFormat:@"BillBoardImage_%d",i]];
            CCSprite * billboardImage = [[CCSprite alloc] initWithCGImage:image.CGImage key:nil];
            
            switch (sprite.tag) {
                case 1:
                    // set position and size for images for level 1
                    billboardImage.scaleX = (110/billboardImage.contentSize.width)*x;
                    billboardImage.scaleY = (50/billboardImage.contentSize.height)*y;
                    
                    billboardImage.position = CGPointMake(115*x,200*y);
                    break;
                    
                case 2:
                    billboardImage.scaleX = (80/billboardImage.contentSize.width)*x;
                    billboardImage.scaleY = (40/billboardImage.contentSize.height)*y;
                    
                    billboardImage.position = CGPointMake(43*x,105*y);

                    break;
                    
                case 3:
                    billboardImage.scaleX = (160/billboardImage.contentSize.width)*x;
                    billboardImage.scaleY = (80/billboardImage.contentSize.height)*y;
                    
                    billboardImage.position = CGPointMake(80*x,215*y);
                    break;
                    
                case 4:
                    billboardImage.scaleX = (160/billboardImage.contentSize.width)*x;
                    billboardImage.scaleY = (80/billboardImage.contentSize.height)*y;
                    
                    billboardImage.position = CGPointMake(80*x,215*y);
                    break;
                    
                default:
                    billboardImage.scaleX = (60/billboardImage.contentSize.width)*x;
                    billboardImage.scaleY = (30/billboardImage.contentSize.height)*y;
                    
                    billboardImage.position = CGPointMake(CGRectGetMidX(sprite.textureRect)*x,CGRectGetMidY(sprite.textureRect)*y) ;
                    break;
            }
            
//            [billboardImage setTextureRect:CGRectMake(0, 0, 43, 21)];
            [sprite addChild:billboardImage];
            
            NSLog(@"h:%f  w:%f",[billboardImage boundingBox].size.height,[billboardImage boundingBox].size.width);
            
            if (i+1 < images.count) {
                i++;
            }
        }
    }
}
+ (void) setIsWaitForAds:(BOOL) isNeedToWait
{
    if (!isNeedToWait) {
        isWaitForAds = isNeedToWait;
        [spritesToAddAds release];
    }
}

@end
