//
//  ADBannerView.h
//  Bus Rage
//
//  Created by Viktor on 20.11.12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BRBannerViewDelegate.h"


@interface BRBannerView : UIView

+ (void) getADBannerView:(NSObject<BRBannerViewDelegate>*) delegate;
@property (retain, nonatomic) IBOutletCollection(UIImageView) NSArray *bannerImageViews;
+ (void) getADBanners:(NSObject<BRBannerViewDelegate>*) delegate;
+ (void) getADImages:(NSObject<BRBannerViewDelegate>*) delegate;
@end
