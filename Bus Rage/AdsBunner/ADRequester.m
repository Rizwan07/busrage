//
//  ADRequester.m
//  Bus Rage
//
//  Created by Viktor on 20.11.12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "ADRequester.h"
#import "ADRequestResult.h"
#import "SBJson.h"

#define URL_REQUEST [NSURL URLWithString: @"http://developer.avenuesocial.com/azeemsal/busrage/application/json/detail.php"]

#define KEY_BANNER_ID @"wall_id"
#define KEY_STATUS @"status"
#define KEY_IMAGE @"image"

@implementation ADRequester

+ (NSArray*) parseArray:(NSArray*) anArray
{
    NSMutableArray * results = [NSMutableArray array];
//    for (NSDictionary * dict in anArray) {
//        NSString * imageUrl = [dict objectForKey:KEY_IMAGE];
//        imageUrl = [imageUrl stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
//        int status = [[dict objectForKey:KEY_STATUS] integerValue];
//        int bannerId = [[dict objectForKey:KEY_BANNER_ID] integerValue];
//        ADRequestResult * result = [[ADRequestResult alloc] initWithImageUrl:[NSURL URLWithString:imageUrl] bannerState:status bannerId:bannerId];
//        [results addObject:result];
//        [result release];
//    }
    NSLog(@"Result: %@",results);
    return results;
}

+ (NSArray*) getADBanners
{
    NSURLRequest * request = [NSURLRequest requestWithURL:URL_REQUEST];
    NSURLResponse * response;
    NSError * error=nil;
    
    NSData * jsonData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
   
    if (error) {
        NSLog(@"Error while requsting :%@",error.debugDescription);
        return nil;
    }
    
    NSString * string = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];
    NSLog(@"%@",string);
    SBJsonParser * parser = [[SBJsonParser alloc] init];
    id result = [parser objectWithString:string];
    
    [string release];
    [parser release];
    
    return [self parseArray:result];
}


@end
