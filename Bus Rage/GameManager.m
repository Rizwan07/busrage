//
//  GameManager.m
//  Electric Bird
//
//  Created by Steve Jobs on 12-2-27.
//  Copyright (c) 2012年 Apple inc. All rights reserved.
//

#import "GameManager.h"
#import <mach/mach.h>
#import "CTarget.h"

@implementation GameManager
@synthesize m_bEnabledSound;
@synthesize m_bEnabledMusic;
@synthesize m_bPlayMusic;

@synthesize m_bMale;
@synthesize m_DriverName;
@synthesize m_levelNum;
@synthesize m_TargetArray, m_TargetNameArray, m_TargetfacebookImageObjectIDArray, m_TargetfacebookImageArray;
@synthesize targetNum1, targetNum2, targetNum3,targetNum4;//, targetNum5, targetNum6,targetNum7;
@synthesize fbTargetImage1, fbTargetImage2,fbTargetImage3,fbTargetImage4;//, fbTargetImage5,fbTargetImage6,fbTargetImage7;
@synthesize bNetWork_Con;
@synthesize m_targetNum;
@synthesize m_GameScore;
@synthesize m_target1HitNum, m_target2HitNum, m_target3HitNum,m_target4HitNum;//, m_target5HitNum, m_target6HitNum, m_target7HitNum;
@synthesize m_loopNum;
@synthesize m_currentLevelTargetNum;

@synthesize isSelectAll;

@synthesize m_currentPassedLevel;

@synthesize challengeId = _challengeId;
@synthesize m_targetDic;

static GameManager* _sharedGameManager = nil;
    
+(GameManager*)sharedGameManager {

    //@synchronized([GameManager class])
    @synchronized(self)
    {
        if (!_sharedGameManager) {
            _sharedGameManager = [[GameManager alloc]init];
        }
    }
    return _sharedGameManager;

    
//    @synchronized([GameManager class])
//    {
//        if (!_sharedGameManager) {
//            [[self alloc]init];
//        }
//        return _sharedGameManager;
//    }
//    return nil;
}

+(id)alloc {
    
    @synchronized([GameManager class]) {
        NSAssert(_sharedGameManager == nil, @"Attempted to allocated a second instance of the Game Manager singleton.");
        _sharedGameManager = [super alloc];
        return _sharedGameManager;
    }
    return nil;
}

- (id) init {
    
    if ((self = [super init])) {
        m_bEnabledMusic = TRUE;
        m_bEnabledSound = TRUE;
        m_bPlayMusic = NO;
        //target init
        m_TargetArray = [[NSMutableArray arrayWithCapacity:1] retain];
        for (int i = 0; i < 29; i++) {
            NSString* strValue = [NSString stringWithFormat:@"0"];
            [m_TargetArray addObject:strValue];
        }
        m_TargetfacebookImageObjectIDArray = [[NSMutableArray alloc] init];
        m_TargetfacebookImageArray = [[NSMutableArray alloc] init];
        for (int i = 0; i < 29; i++) 
        {
            NSString *tmpString = [NSString stringWithFormat:@""];
            [m_TargetfacebookImageObjectIDArray addObject:tmpString];            
            UIImage *image = [UIImage imageNamed:@"thumbnail_unselect_BG.png"];
            [m_TargetfacebookImageArray addObject:image];
        }
        m_TargetNameArray = [[NSMutableArray alloc] init];        
        for (int i = 0; i < 29; i++)
        {
            NSString *tmpString = [NSString stringWithFormat:@""];
            [m_TargetNameArray addObject:tmpString];            
        }        

    }
    return self;
}

- (void) dealloc {      
    [m_TargetArray removeAllObjects];
    [m_TargetArray release];
    [m_TargetNameArray removeAllObjects];
    [m_TargetNameArray release];
    [m_TargetfacebookImageObjectIDArray removeAllObjects];
    [m_TargetfacebookImageObjectIDArray release];
    [m_TargetfacebookImageArray removeAllObjects];
    [m_TargetfacebookImageArray release];    

    [super dealloc];    
}

- (NSString *)createEditableCopyOfFileIfNeeded:(NSString *)_filename {//kgh 2_21
    // First, test for existence.
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableFilePath = [documentsDirectory stringByAppendingPathComponent: _filename ];
	
    //NSLog(@"writableFilePath:%@", writableFilePath);
    return writableFilePath;
}

- (void)saveCompleteLevel
{
    NSString *filePath = [self createEditableCopyOfFileIfNeeded:@"settings"];
	//NSLog(@"filePath:%@", filePath);
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:[NSNumber numberWithInt:self.m_currentPassedLevel] forKey:@"currentPassedLevel"];
	if(![dict writeToFile:filePath atomically:YES]){
		NSLog(@"SETTINGS WRITE ERROR");
        [dict release];
        return;
	}
    [dict release];
	//NSLog(@"%@", dict);
}
- (void)loadCompleteLevel
{
    NSString *filePath = [self createEditableCopyOfFileIfNeeded:@"settings"];
	
	NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
	if (dict) {	 
        self.m_currentPassedLevel = [[dict objectForKey:@"currentPassedLevel"] intValue]; 

	}
    else{
        self.m_currentPassedLevel = 0; 
        
        [self saveCompleteLevel]; 
    }
	NSLog(@"[settings controller] loading complete");
    //self.m_currentPassedLevel = 4;
}

@end
