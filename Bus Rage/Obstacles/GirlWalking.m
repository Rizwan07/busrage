//
//  GirlWalking.m
//  Bus Rage
//
//  Created by Jin Tie on 6/10/12.
//  Copyright 2012 k. All rights reserved.
//

#import "GirlWalking.h"


@implementation GirlWalking
//@synthesize m_Sprite;
@synthesize m_distance;
@synthesize m_bBusHit;
@synthesize m_road_width;
//@synthesize m_animWalk;

- (id) init {
    
    CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
    NSMutableArray *frames = [NSMutableArray array];
    NSString *fileName;
    CCSpriteFrame *frame;
    CCAnimation *animation;
    
    self = [super init];
    if (self) {        
        m_Sprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"girlwalk_%i.png", 1]];
        [self addChild:m_Sprite z:0]; 
        
        [frames removeAllObjects];
        for (int i = 1; i <= 3; i++)
        {
            fileName = [NSString stringWithFormat:@"girlwalk_%i.png", i];
            frame = [frameCache spriteFrameByName:fileName];
            [frames addObject:frame];
        }
        fileName = [NSString stringWithFormat:@"girlwalk_%i.png", 2];
        frame = [frameCache spriteFrameByName:fileName];
        [frames addObject:frame];
        animation = [CCAnimation animationWithFrames:frames delay: 0.1f];
		m_animWalk = [CCAnimate actionWithAnimation:animation];
        [frames removeAllObjects]; 
    }
    return self;
}
- (void) dealloc
{       
    [super dealloc];
}
- (void) setAnimWalk
{
    CCRepeatForever *forever = [CCRepeatForever actionWithAction:m_animWalk];
    [m_Sprite runAction:forever];    
}
@end
