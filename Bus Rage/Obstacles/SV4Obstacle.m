//
//  SV4Obstacle.m
//  Bus Rage
//
//  Created by Jin Tie on 6/10/12.
//  Copyright 2012 k. All rights reserved.
//

#import "SV4Obstacle.h"


@implementation SV4Obstacle
//@synthesize m_Sprite;
@synthesize m_distance;
@synthesize m_bBusHit;
@synthesize m_road_width;

- (id) init:(int)num {
    
    self = [super init];
    if (self) {
        switch (num) {
            case 1:
                m_Sprite = [CCSprite spriteWithFile:@"ob_Baby_Stroller_SV.png"];
                break;
            case 2:
                m_Sprite = [CCSprite spriteWithFile:@"ob_WIPmanhole_sign_SV.png"];
                break;  
            case 3:
                m_Sprite = [CCSprite spriteWithFile:@"ob_wheel_chair_SV.png"];
                break;
            case 4:
                m_Sprite = [CCSprite spriteWithFile:@"ob_teenager-skateboard_SV.png"];
                break;
            case 5:
                m_Sprite = [CCSprite spriteWithFile:@"ob_hotdog_troly_SV.png"];
                break;
            case 6:
                m_Sprite = [CCSprite spriteWithFile:@"ob_cone_SA.png"];
                break;
            case 7:
                m_Sprite = [CCSprite spriteWithFile:@"ob_Shopping_trolley_SA.png"];
                break;
            case 8:
                m_Sprite = [CCSprite spriteWithFile:@"oil barrel_FB.png"];
                m_Sprite.scale = 0.6;
                break;
            case 9:
                m_Sprite = [CCSprite spriteWithFile:@"oil barrel_2_FB.png"];
                m_Sprite.scale = 0.6;
                break;
            default:
                m_Sprite = [CCSprite spriteWithFile:@"ob_hotdog_troly_SV.png"];
                break;
        }
        
        [self addChild:m_Sprite z:0];       
    }
    return self;
}
- (void) dealloc
{       
    [super dealloc];
}
@end
