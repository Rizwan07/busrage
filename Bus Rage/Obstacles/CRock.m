//
//  CRock.m
//  Bus Rage
//
//  Created by Jin Tie on 6/10/12.
//  Copyright 2012 k. All rights reserved.
//

#import "CRock.h"


@implementation CRock
//@synthesize m_Sprite;
@synthesize m_distance;
@synthesize m_bBusHit;
@synthesize m_road_width;

- (id) init:(int)num forIpad:(BOOL)isIpad {
    
    self = [super init];
    if (self) {
        if (isIpad) {
            switch (num) {
                case 1:
                    m_Sprite = [CCSprite spriteWithFile:@"Rock_1_MV_ipad.png"];
                    break;
                case 2:
                    m_Sprite = [CCSprite spriteWithFile:@"Rock_2_MV_ipad.png"];
                    break;    
                default:
                    m_Sprite = [CCSprite spriteWithFile:@"Rock_1_MV_ipad.png"];
                    break;
            }
        }
        else {
            switch (num) {
                case 1:
                    m_Sprite = [CCSprite spriteWithFile:@"ob_Rock_1_MV.png"];
                    break;
                case 2:
                    m_Sprite = [CCSprite spriteWithFile:@"ob_Rock_2_MV.png"];
                    break;    
                default:
                    m_Sprite = [CCSprite spriteWithFile:@"ob_Rock_1_MV.png"];
                    break;
            }
        }
        [self addChild:m_Sprite z:0];       
    }
    return self;
}
- (void) dealloc
{       
    [super dealloc];
}
@end
